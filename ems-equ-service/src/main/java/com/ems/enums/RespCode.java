package com.ems.enums;

import java.util.Objects;

/**
 * 响应异常码统一管理表(业务异常码规约，前2位：模块编号，后3位：异常编号
 * 
 * @author chenzhao @date Oct 16, 2019
 */
public enum RespCode {
	// #公共模块
	// 编码前缀：00----------------------------------------------------------------------------------------------
	/**
	 * "0", "success"
	 */
	SUCCESS("0", "success"),

	/**
	 * 参数非法
	 */
	PARAM_ILLEGAL("00001", "参数非法"), PARAM_INCOMPLETE("00002", "必要参数[%s]残缺"),
	REQ_SIGN_ERROR("00003", "验签失败"),

	REQ_VERSION_ERROR("00004", "请求服务version异常，不存在此版本服务"),
	/**
	 * 外部服务请求超时
	 */
	RESTFUL_REQ_TIMEOUT("00005", "外部服务请求超时"),
	/**
	 * 外部服务请求业务异常,响应报文：%s
	 */
	RESTFUL_REQ_SERVICEERROR("00006", "%s"),
	/**
	 * 外部服务响应报文为空
	 */
	RESTFUL_REQ_RESPERROR("00007", "外部服务响应报文为空"),
	/**
	 * "00008", "用户未登录"
	 */
	TOKEN_INVALID("00008", "用户未登录"),
	/**
	 * "00011", "手机号非法"
	 */
	PHONE_INVALID("00011", "手机号非法"),
	/**
	 * "00012", "邮箱地址非法"
	 */
	EMAIL_INVALID("00012", "邮箱地址非法"),
	// #大屏监控+整体监控
	// 编码前缀：01----------------------------------------------------------------------------------------------
	/**
	 * "01001", "查询时间区间过长，不得超过365天"
	 */
	monitor_timeinterval_out("01001", "查询时间区间过长，不得超过365天"),

	// #大屏监控+整体监控
	// 编码前缀：01----------------------------------------------------------------------------------------------
	/**
	 * "04001", "模式参数保存失败，未全部成功"
	 */
	operator_save_error("04001", "模式参数保存失败，未全部成功"),
	/**
	 * "04004", "设备通讯失败，指令未成功"
	 */
	operator_control_error("04004", "设备通讯失败，指令未成功"),
	/**
	 * "04009", "设备类型不存在"
	 */
	operator_equType_unexist("04009", "设备类型不存在"),
	/**
	 * "04013", "设备配置未初始化"
	 */
	operator_equ_unInit("04013", "设备配置未初始化"),

	// #最后一个结束----------------------------------------------------------------------------------
	/**
	 * "9", "服务繁忙"
	 */
	FAILURE("9", "服务繁忙");

	private String code;
	private String msg;

	RespCode(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public static RespCode getRespByCode(String code) {
		if (code == null) {
			return null;
		}
		for (RespCode resp : values()) {
			if (resp.getCode().equals(code)) {
				return resp;
			}
		}
		throw new IllegalArgumentException("无效的code值!code:" + code);
	}

	public String getCode() {
		return code;
	}

	public String getMsg() {
		return msg;
	}

	public boolean isSuccess(String code) {
		return Objects.equals(code, this.code);
	}
}
