# EMS(多功能电能表的采集系统)

#### 介绍
对接多功能电能表，采集电表的各种电能相关数据，比如电流、电压、电功率等等（DLT645协议编解码通讯）
并对采集数据做图形化展示

#### 软件架构
使用NIO和电能表异步通讯
spring boot、Cloud、Zuul

#### 使用说明

1. DLT645-protocol是电能表的编解码实现
2. ems-equ-service是和电能表通讯的服务端实现
3. ems-gatewayZuul网关
4. ems-web是对采集数据做图形化展示

