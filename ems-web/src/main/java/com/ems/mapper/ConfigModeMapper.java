/**
 * 
 */
package com.ems.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import com.ems.entity.ConfigMode;

/**
 * 模式设置，也可用于字典存键值对，通过类型隔了区分数据类型
 * 
 * @author chenzhao @date Oct 18, 2019
 */
@Repository
@Mapper
public interface ConfigModeMapper {

	@Select(value = "<script>select `id`,`type`,`key`,`value`" + " from config_mode" + "<where>" 
	+ " and `type` = #{type} "
	+ "</where></script>")
	public List<ConfigMode> selectList4Type(@Param(value = "type") String type);

	@Select(value = "<script>select `id`,`type`,`key`,`value`" + " from config_mode" + "<where>" 
	+ " and `type` = #{type} "
	+ "<if test=\"key != null and key != '' \">" + " and `key` = #{key} " + "</if>"
	+ "</where></script>")
	public ConfigMode selectOne(@Param(value = "type") String type, @Param(value = "key") String key);
	
	@Insert(value = "INSERT INTO config_mode" + " (`type`,`key`,`value`) " 
	+ " VALUES (#{type},#{key},#{value})")
	public int insert(@Param(value = "type") String type, @Param(value = "key") String key, @Param(value = "value") String value);
	
	@Update(value = "<script>UPDATE config_mode SET  `id`=`id`"
	+ "<if test=\"key != null and key != '' \">" + ",`key` = #{key}" + "</if>"
	+ "<if test=\"value != null and value != '' \">" + ",`value` = #{value}" + "</if>"
	+ "<where>" 
	+ "<if test=\"type != null and type != '' \">" + " and `type` = #{type} " + "</if>"
	+ "<if test=\"key != null and key != '' \">" + " and `key` = #{key} " + "</if>"
	+ "</where>"
	+ "</script>")
	public int update(@Param(value = "type") String type, @Param(value = "key") String key, @Param(value = "value") String value);

}
