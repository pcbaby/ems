/**
 * 
 */
package com.ems.mapper;


import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import com.ems.entity.ConfigEquRun;

/**
 * 设备配置数据+最新采集的运行数据
 * 
 * @author chenzhao @date Oct 17, 2019
 */
@Repository
@Mapper
public interface ConfigEquRunMapper {

	@Select(value = "<script>select id,type,equ_id equId,switch_status switchStatus,max_power maxPower,real_time_power realTimePower,real_time_current0 realTimeCurrent0,real_time_current1 realTimeCurrent1,real_time_current2 realTimeCurrent2,real_time_voltage0 realTimeVoltage0,real_time_voltage1 realTimeVoltage1,real_time_voltage2 realTimeVoltage2,citygrid_set_power_low citygridSetPowerLow,citygrid_set_power_high citygridSetPowerHigh" + " from config_equ_run" + "<where>"
			+" and type = 'cityGrid' limit 1"
			+ "</where></script>")
	public Map<String,Object> selectCtiy();

	@Update(value = "<script>UPDATE config_equ_run SET  id=id"
			+ "<if test=\"switchStatus != null  \">" + ",switch_status = #{switchStatus}" + "</if>"
			+ "<if test=\"maxPower != null  \">" + ",max_power = #{maxPower}" + "</if>"
			+ "<if test=\"realTimePower != null  \">" + ",real_time_power = #{realTimePower}" + "</if>"
			+ "<if test=\"realTimeCurrent != null  \">" + ",real_time_current = #{realTimeCurrent}" + "</if>"
			+ "<if test=\"realTimeVoltage != null  \">" + ",real_time_voltage = #{realTimeVoltage}" + "</if>"
			+ " where type = 'cityGrid' "
			+ "<if test=\"equId != null and equId != '' \">" + ",equ_id = #{equId}" + "</if>"
			+ "</script>")
	public int updateCity(ConfigEquRun configEquRun);

	@Select(value = "<script>select id,type,equ_id equId,switch_status switchStatus,max_power maxPower,real_time_power realTimePower,real_time_current0 realTimeCurrent0,real_time_current1 realTimeCurrent1,real_time_current2 realTimeCurrent2,real_time_voltage0 realTimeVoltage0,real_time_voltage1 realTimeVoltage1,real_time_voltage2 realTimeVoltage2,stored_real_time_soc storedRealTimeSoc,stored_real_time_capacity storedRealTimeCapacity,stored_charge_status storedChargeStatus,stored_max_charge_power storedMaxChargePower,stored_max_discharge_power storedMaxDischargePower,stored_soc storedSoc,stored_soh storedSoh" + " from config_equ_run" + "<where>"
			+" and type = 'storedEnergy' limit 1"
			+ "</where></script>")
	public Map<String,Object> selectStored();
	
	@Select(value = "<script>select id,equ_id equId,switch_status switchStatus,max_power maxPower,real_time_power realTimePower,real_time_current0 realTimeCurrent0,real_time_current1 realTimeCurrent1,real_time_current2 realTimeCurrent2,real_time_voltage0 realTimeVoltage0,real_time_voltage1 realTimeVoltage1,real_time_voltage2 realTimeVoltage2,stored_real_time_soc storedRealTimeSoc,stored_real_time_capacity storedRealTimeCapacity,stored_charge_status storedChargeStatus,stored_max_charge_power storedMaxChargePower,stored_max_discharge_power storedMaxDischargePower,stored_soc storedSoc,stored_soh storedSoh"
			+ ",stored_provider storedProvider,stored_device_type storedDeviceType,stored_max_energy storedMaxEnergy,stored_install_time storedInstallTime,stored_install_position storedInstallPosition,stored_grid_voltage storedGridVoltage,stored_grid_current storedGridCurrent,stored_eps_current storedEpsCurrent,stored_battery_voltage storedBatteryVoltage,stored_battery_current storedBatteryCurrent,stored_daily_charge_energy storedDailyChargeEnergy,stored_daily_discharge_energy storedDailyDischargeEnergy,stored_total_charge_energy storedTotalChargeEnergy,stored_total_discharge_energy storedTotalDischargeEnergy,stored_daily_eps_discharge_energy storedDailyEpsDischargeEnergy,stored_total_eps_discharge_energy storedTotalEpsDischargeEnergy,stored_single_device storedSingleDevice" + " from config_equ_run" + "<where>"
			+" and type = 'storedEnergy' limit 1"
			+ "</where></script>")
	public Map<String,Object> selectStored4single();

	@Update(value = "<script>UPDATE config_equ_run SET  id=id"
			+ "<if test=\"switchStatus != null  \">" + ",switch_status = #{switchStatus}" + "</if>"
			+ "<if test=\"maxPower != null  \">" + ",max_power = #{maxPower}" + "</if>"
			+ "<if test=\"realTimePower != null \">" + ",real_time_power = #{realTimePower}" + "</if>"
			+ "<if test=\"realTimeCurrent != null  \">" + ",real_time_current = #{realTimeCurrent}" + "</if>"
			+ "<if test=\"realTimeVoltage != null  \">" + ",real_time_voltage = #{realTimeVoltage}" + "</if>"
			+ " where type = 'storedEnergy' "
			+ "<if test=\"equId != null and equId != '' \">" + ",equ_id = #{equId}" + "</if>"
			+ "</script>")
	public int updateStored(ConfigEquRun configEquRun);

	@Select(value = "<script>select id,type,equ_id equId,switch_status switchStatus,max_power maxPower,real_time_power realTimePower,real_time_current0 realTimeCurrent0,real_time_current1 realTimeCurrent1,real_time_current2 realTimeCurrent2,real_time_voltage0 realTimeVoltage0,real_time_voltage1 realTimeVoltage1,real_time_voltage2 realTimeVoltage2,pv_capacity pvCapacity,real_time_current0 realTimeCurrent0,real_time_current1 realTimeCurrent1,real_time_current2 realTimeCurrent2,real_time_voltage0 realTimeVoltage0,real_time_voltage1 realTimeVoltage1,real_time_voltage2 realTimeVoltage2" + " from config_equ_run" + "<where>"
			+" and type = 'PV' limit 1"
			+ "</where></script>")
	public Map<String,Object> selectPv();

	@Update(value = "<script>UPDATE config_equ_run SET  id=id"
			+ "<if test=\"switchStatus != null  \">" + ",switch_status = #{switchStatus}" + "</if>"
			+ "<if test=\"maxPower != null  \">" + ",max_power = #{maxPower}" + "</if>"
			+ "<if test=\"realTimePower != null \">" + ",real_time_power = #{realTimePower}" + "</if>"
			+ "<if test=\"realTimeCurrent != null  \">" + ",real_time_current = #{realTimeCurrent}" + "</if>"
			+ "<if test=\"realTimeVoltage != null  \">" + ",real_time_voltage = #{realTimeVoltage}" + "</if>"
			+ " where type = 'PV' "
			+ "<if test=\"equId != null and equId != '' \">" + ",equ_id = #{equId}" + "</if>"
			+ "</script>")
	public int updatePv(ConfigEquRun configEquRun);
	
	@Select(value = "<script>select id,type,equ_id equId,switch_status switchStatus,max_power maxPower,real_time_power realTimePower,real_time_current0 realTimeCurrent0,real_time_current1 realTimeCurrent1,real_time_current2 realTimeCurrent2,real_time_voltage0 realTimeVoltage0,real_time_voltage1 realTimeVoltage1,real_time_voltage2 realTimeVoltage2" + " from config_equ_run" + "<where>"
			+" and type = 'V2L' limit 1"
			+ "</where></script>")
	public Map<String,Object> selectV2L();

	@Update(value = "<script>UPDATE config_equ_run SET  id=id"
			+ "<if test=\"switchStatus != null  \">" + ",switch_status = #{switchStatus}" + "</if>"
			+ "<if test=\"maxPower != null  \">" + ",max_power = #{maxPower}" + "</if>"
			+ "<if test=\"realTimePower != null \">" + ",real_time_power = #{realTimePower}" + "</if>"
			+ "<if test=\"realTimeCurrent != null  \">" + ",real_time_current = #{realTimeCurrent}" + "</if>"
			+ "<if test=\"realTimeVoltage != null  \">" + ",real_time_voltage = #{realTimeVoltage}" + "</if>"
			+ " where type = 'V2L' "
			+ "<if test=\"equId != null and equId != '' \">" + ",equ_id = #{equId}" + "</if>"
			+ "</script>")
	public int updateV2L(ConfigEquRun configEquRun);

	@Select(value = "<script>select id,type,equ_id equId,switch_status switchStatus,max_power maxPower,real_time_power realTimePower,real_time_current0 realTimeCurrent0,real_time_current1 realTimeCurrent1,real_time_current2 realTimeCurrent2,real_time_voltage0 realTimeVoltage0,real_time_voltage1 realTimeVoltage1,real_time_voltage2 realTimeVoltage2" + " from config_equ_run" + "<where>"
			+" and type = 'DC' limit 1"
			+ "</where></script>")
	public Map<String,Object> selectDc();

	@Update(value = "<script>UPDATE config_equ_run SET  id=id"
			+ "<if test=\"switchStatus != null  \">" + ",switch_status = #{switchStatus}" + "</if>"
			+ "<if test=\"maxPower != null  \">" + ",max_power = #{maxPower}" + "</if>"
			+ "<if test=\"realTimePower != null \">" + ",real_time_power = #{realTimePower}" + "</if>"
			+ "<if test=\"realTimeCurrent != null  \">" + ",real_time_current = #{realTimeCurrent}" + "</if>"
			+ "<if test=\"realTimeVoltage != null  \">" + ",real_time_voltage = #{realTimeVoltage}" + "</if>"
			+ " where type = 'DC' "
			+ "<if test=\"equId != null and equId != '' \">" + ",equ_id = #{equId}" + "</if>"
			+ "</script>")
	public int updateDc(ConfigEquRun configEquRun);

	@Select(value = "<script>select id,type,equ_id equId,switch_status switchStatus,max_power maxPower,real_time_power realTimePower,real_time_current0 realTimeCurrent0,real_time_current1 realTimeCurrent1,real_time_current2 realTimeCurrent2,real_time_voltage0 realTimeVoltage0,real_time_voltage1 realTimeVoltage1,real_time_voltage2 realTimeVoltage2" + " from config_equ_run" + "<where>"
			+" and type = 'AC' limit 1"
			+ "</where></script>")
	public Map<String,Object> selectAc();

	@Update(value = "<script>UPDATE config_equ_run SET  id=id"
			+ "<if test=\"switchStatus != null  \">" + ",switch_status = #{switchStatus}" + "</if>"
			+ "<if test=\"maxPower != null  \">" + ",max_power = #{maxPower}" + "</if>"
			+ "<if test=\"realTimePower != null \">" + ",real_time_power = #{realTimePower}" + "</if>"
			+ "<if test=\"realTimeCurrent != null  \">" + ",real_time_current = #{realTimeCurrent}" + "</if>"
			+ "<if test=\"realTimeVoltage != null  \">" + ",real_time_voltage = #{realTimeVoltage}" + "</if>"
			+ " where type = 'AC' "
			+ "<if test=\"equId != null and equId != '' \">" + ",equ_id = #{equId}" + "</if>"
			+ "</script>")
	public int updateAc(ConfigEquRun configEquRun);

	@Select(value = "<script>select id,type,equ_id equId,switch_status switchStatus,max_power maxPower,real_time_power realTimePower,real_time_current0 realTimeCurrent0,real_time_current1 realTimeCurrent1,real_time_current2 realTimeCurrent2,real_time_voltage0 realTimeVoltage0,real_time_voltage1 realTimeVoltage1,real_time_voltage2 realTimeVoltage2" + " from config_equ_run" + "<where>"
			+" and type = 'normalLoad' limit 1"
			+ "</where></script>")
	public Map<String,Object> selectNormalLoad();

	@Update(value = "<script>UPDATE config_equ_run SET  id=id"
			+ "<if test=\"switchStatus != null  \">" + ",switch_status = #{switchStatus}" + "</if>"
			+ "<if test=\"maxPower != null  \">" + ",max_power = #{maxPower}" + "</if>"
			+ "<if test=\"realTimePower != null \">" + ",real_time_power = #{realTimePower}" + "</if>"
			+ "<if test=\"realTimeCurrent != null  \">" + ",real_time_current = #{realTimeCurrent}" + "</if>"
			+ "<if test=\"realTimeVoltage != null  \">" + ",real_time_voltage = #{realTimeVoltage}" + "</if>"
			+ " where type = 'normalLoad' "
			+ "<if test=\"equId != null and equId != '' \">" + ",equ_id = #{equId}" + "</if>"
			+ "</script>")
	public int updateNormalLoad(ConfigEquRun configEquRun);
	
	@Select(value = "<script>select id,type,equ_id equId,switch_status switchStatus,max_power maxPower,real_time_power realTimePower,real_time_current0 realTimeCurrent0,real_time_current1 realTimeCurrent1,real_time_current2 realTimeCurrent2,real_time_voltage0 realTimeVoltage0,real_time_voltage1 realTimeVoltage1,real_time_voltage2 realTimeVoltage2" + " from config_equ_run" + "<where>"
			+" and type = 'highPower' limit 1"
			+ "</where></script>")
	public Map<String,Object> selectHighpower();

	@Update(value = "<script>UPDATE config_equ_run SET  id=id"
			+ "<if test=\"switchStatus != null  \">" + ",switch_status = #{switchStatus}" + "</if>"
			+ "<if test=\"maxPower != null  \">" + ",max_power = #{maxPower}" + "</if>"
			+ "<if test=\"realTimePower != null \">" + ",real_time_power = #{realTimePower}" + "</if>"
			+ "<if test=\"realTimeCurrent != null  \">" + ",real_time_current = #{realTimeCurrent}" + "</if>"
			+ "<if test=\"realTimeVoltage != null  \">" + ",real_time_voltage = #{realTimeVoltage}" + "</if>"
			+ " where type = 'highPower' "
			+ "<if test=\"equId != null and equId != '' \">" + ",equ_id = #{equId}" + "</if>"
			+ "</script>")
	public int updateHighpower(ConfigEquRun configEquRun);
	
	@Update(value = "<script>UPDATE config_equ_run SET  id=id"
			+ "<if test=\"switchStatus != null  \">" + ",switch_status = #{switchStatus}" + "</if>"
			+ "<if test=\"maxPower != null  \">" + ",max_power = #{maxPower}" + "</if>"
			+ "<if test=\"realTimePower != null  \">" + ",real_time_power = #{realTimePower}" + "</if>"
			+ "<if test=\"realTimePowerUseless != null  \">" + ",real_time_power_useless = #{realTimePowerUseless}" + "</if>"
			+ "<if test=\"realTimePowerView != null  \">" + ",real_time_power_view = #{realTimePowerView}" + "</if>"
			+ "<if test=\"realTimeEnergy != null  \">" + ",real_time_energy = #{realTimeEnergy}" + "</if>"
			+ "<if test=\"realTimeEnergyForward != null  \">" + ",real_time_energy_forward = #{realTimeEnergyForward}" + "</if>"
			+ "<if test=\"realTimeEnergyReverse != null  \">" + ",real_time_energy_reverse = #{realTimeEnergyReverse}" + "</if>"
			+ "<if test=\"realTimeCurrent0 != null  \">" + ",real_time_current0 = #{realTimeCurrent0}" + "</if>"
			+ "<if test=\"realTimeVoltage0 != null  \">" + ",real_time_voltage0 = #{realTimeVoltage0}" + "</if>"
			+ "<if test=\"realTimeCurrent1 != null  \">" + ",real_time_current1 = #{realTimeCurrent1}" + "</if>"
			+ "<if test=\"realTimeVoltage1 != null  \">" + ",real_time_voltage1 = #{realTimeVoltage1}" + "</if>"
			+ "<if test=\"realTimeCurrent2 != null  \">" + ",real_time_current2 = #{realTimeCurrent2}" + "</if>"
			+ "<if test=\"realTimeVoltage2 != null  \">" + ",real_time_voltage2 = #{realTimeVoltage2}" + "</if>"
			+ "<if test=\"storedChargeStatus != null  \">" + ",stored_charge_status = #{storedChargeStatus}" + "</if>"
			+ " where type = #{type} "
			+ "<if test=\"equId != null and equId != '' \">" + ",equ_id = #{equId}" + "</if>"
			+ "</script>")
	public int update(ConfigEquRun configEquRun);

}
