/**
 * 
 */
package com.ems.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import com.ems.entity.EquPower;

/**
 * 设备运行功率-增量采集日志
 * 
 * @author chenzhao @date Oct 17, 2019
 */
@Repository
@Mapper
public interface EquPowerMapper {

	@Select(value = "<script>select id,power,collect_time,collect_timestamp" + " from equ_log_citygrid" + "<where>"
			+ "<if test=\"startTime != null and startTime != '' \">"
			+ " and collect_timestamp between #{startTime} and #{endTime}" + "</if>" + "</where></script>")
	public List<EquPower> selectCtiy(@Param(value = "startTime") long startTime,
			@Param(value = "endTime") long endTime);

	@Insert(value = "INSERT INTO equ_log_citygrid" + " (equ_id,power,collect_time,collect_timestamp) "
			+ " VALUES (#{equId},#{power},NOW(),#{collectTimestamp})")
	public int insertCity(EquPower equPower);

	@Select(value = "<script>select id,power,collect_time,collect_timestamp,charge_status" + " from equ_log_storedenergy"
			+ "<where>" + "<if test=\"startTime != null and startTime != '' \">"
			+ " and collect_timestamp between #{startTime} and #{endTime}" + "</if>" + "</where></script>")
	public List<EquPower> selectStored(@Param(value = "startTime") long startTime,
			@Param(value = "endTime") long endTime);

	@Insert(value = "INSERT INTO equ_log_storedenergy" + " (equ_id,power,collect_time,collect_timestamp,charge_status"
			+ ",base_status,total_voltage,temperature,soc,cycle_time,max_charge_voltageofpile,max_charge_current,min_discharge_voltageofpile,max_discharge_current,soh,remain_capacity,charge_capactityofpile,discharge_capacityofpile,daily_accumulate_charge_capactiy,daily_accumulate_discharge_capactiy,history_accumulate_charge_capacity,history_accumulate_discharge_capacity,force_charge_request_mark,balance_charge_request_mark,error_codes1,error_codes2,module_number_in_series,cell_number_in_series,charge_forbidden_mark,discharge_forbidden_mark,pile_voltage,pile_temperature,pile_current"
			+ ") "
			+ " VALUES (#{equId},#{power},NOW(),#{collectTimestamp},#{chargeStatus}"
			+ ",#{baseStatus},#{totalVoltage},#{temperature},#{soc},#{cycleTime},#{maxChargeVoltageofpile},#{maxChargeCurrent},#{minDischargeVoltageofpile},#{maxDischargeCurrent},#{soh},#{remainCapacity},#{chargeCapactityofpile},#{dischargeCapacityofpile},#{dailyAccumulateChargeCapactiy},#{dailyAccumulateDischargeCapactiy},#{historyAccumulateChargeCapacity},#{historyAccumulateDischargeCapacity},#{forceChargeRequestMark},#{balanceChargeRequestMark},#{errorCodes1},#{errorCodes2},#{moduleNumberInSeries},#{cellNumberInSeries},#{chargeForbiddenMark},#{dischargeForbiddenMark},#{pileVoltage},#{pileTemperature},#{pileCurrent}"
			+ ")")
	public int insertStored(EquPower equPower);

	@Select(value = "<script>select id,power,collect_time,collect_timestamp" + " from equ_log_pv" + "<where>"
			+ "<if test=\"startTime != null and startTime != '' \">"
			+ " and collect_timestamp between #{startTime} and #{endTime}" + "</if>" + "</where></script>")
	public List<EquPower> selectPv(@Param(value = "startTime") long startTime,
			@Param(value = "endTime") long endTime);

	@Insert(value = "INSERT INTO equ_log_pv" + " (equ_id,power,collect_time,collect_timestamp) "
			+ " VALUES (#{equId},#{power},NOW(),#{collectTimestamp})")
	public int insertPv(EquPower equPower);

	@Select(value = "<script>select id,power,collect_time,collect_timestamp" + " from equ_log_dc" + "<where>"
			+ "<if test=\"startTime != null and startTime != '' \">"
			+ " and collect_timestamp between #{startTime} and #{endTime}" + "</if>" + "</where></script>")
	public List<EquPower> selectDc(@Param(value = "startTime") long startTime,
			@Param(value = "endTime") long endTime);

	@Insert(value = "INSERT INTO equ_log_dc" + " (equ_id,power,collect_time,collect_timestamp) "
			+ " VALUES (#{equId},#{power},NOW(),#{collectTimestamp})")
	public int insertDc(EquPower equPower);

	@Select(value = "<script>select id,power,collect_time,collect_timestamp" + " from equ_log_ac" + "<where>"
			+ "<if test=\"startTime != null and startTime != '' \">"
			+ " and collect_timestamp between #{startTime} and #{endTime}" + "</if>" + "</where></script>")
	public List<EquPower> selectAc(@Param(value = "startTime") long startTime,
			@Param(value = "endTime") long endTime);

	@Insert(value = "INSERT INTO equ_log_ac" + " (equ_id,power,collect_time,collect_timestamp) "
			+ " VALUES (#{equId},#{power},NOW(),#{collectTimestamp})")
	public int insertAc(EquPower equPower);

	@Select(value = "<script>select id,power,collect_time,collect_timestamp" + " from equ_log_highpower"
			+ "<where>" + "<if test=\"startTime != null and startTime != '' \">"
			+ " and collect_timestamp between #{startTime} and #{endTime}" + "</if>" + "</where></script>")
	public List<EquPower> selectHigntpower(@Param(value = "startTime") long startTime,
			@Param(value = "endTime") long endTime);

	@Insert(value = "INSERT INTO equ_log_highpower" + " (equ_id,power,collect_time,collect_timestamp) "
			+ " VALUES (#{equId},#{power},NOW(),#{collectTimestamp})")
	public int insertHigntpower(EquPower equPower);

//	@Update(value = "<script>UPDATE user SET  uptime=NOW(),upcount=upcount+1"
//			+ "<if test=\"wxUnionid != null and wxUnionid != '' \">" + ",wx_unionid = #{wxUnionid}" + "</if>"
//			+ " where phone = #{phone} and wxUnionid <![CDATA[<>]]> wxOpenid</script>")
//	public int updateUnionid(User user);

}
