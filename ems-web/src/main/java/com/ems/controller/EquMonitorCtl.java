/**
 * 
 */
package com.ems.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.ems.common.ServiceException;
import com.ems.config.annotation.CheckSign;
import com.ems.entity.EquPower;
import com.ems.enums.RespCode;
import com.ems.service.ConfigEquRunService;
import com.ems.service.EquPowerService;
import com.ems.utils.RespUtil;

/**
 * 设备监控：大屏监控、整体监控、日常管理查询
 * 
 * @author chenzhao @date Oct 17, 2019
 */
@RequestMapping("monitor")
@RestController
public class EquMonitorCtl {

	@Autowired
	EquPowerService equPowerServicer;

	@Autowired
	ConfigEquRunService configEquRunService;

	/**
	 * 整体监控
	 * 
	 * @author chenzhao @date Oct 17, 2019
	 * @param version
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("data")
	@CheckSign(isCheckLogin = true)
	public Object monitorListData(@RequestHeader("version") String version, @RequestBody String reqStr)
			throws Exception {
		if ("v1".equals(version)) {
			JSONObject req=JSONObject.parseObject(reqStr);
			List<EquPower> listEquPower = equPowerServicer.monitorListDataV1(req.getString("type"),
					req.getLongValue("startTime"), req.getLongValue("endTime"));
			return RespUtil.listResp(listEquPower);
		} else {
			throw new ServiceException(RespCode.REQ_VERSION_ERROR);
		}

	}
	
	/**
	 * 储能监控
	 * @author ack @date Oct 22, 2019
	 * @param version
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("storedEnergy")
	@CheckSign(isCheckLogin = true)
	public Object storedEnergy(@RequestHeader("version") String version)
			throws Exception {
		if ("v1".equals(version)) {
			return RespUtil.dataResp(configEquRunService.selectStored4single());
		} else {
			throw new ServiceException(RespCode.REQ_VERSION_ERROR);
		}

	}


}
