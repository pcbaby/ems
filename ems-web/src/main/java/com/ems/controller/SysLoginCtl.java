/**
 * 
 */
package com.ems.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.ems.config.annotation.CheckSign;
import com.ems.entity.SysRole;
import com.ems.entity.SysUser;
import com.ems.service.SysLoginService;
import com.ems.utils.RespUtil;

/**
 * 系统登录
 * 
 * @author ack @date Oct 19, 2019
 *
 */
@RestController
public class SysLoginCtl {

	@Autowired
	SysLoginService sysLoginService;

	/**
	 * 用户登录
	 * 
	 * @author ack @date Oct 19, 2019
	 * @param version
	 * @param req
	 * @return
	 */
	@RequestMapping("user/login")
	@CheckSign(isCheckLogin = false)
	public Object login(@RequestHeader("version") String version, @RequestBody String req) {
		return RespUtil.dataResp(sysLoginService.userLogin(JSONObject.parseObject(req, SysUser.class)));
	}

	/**
	 * 用户注销
	 * 
	 * @author ack @date Oct 19, 2019
	 * @param version
	 * @param req
	 * @return
	 */
	@RequestMapping("user/logout")
	@CheckSign()
	public Object logout(@RequestHeader("version") String version, @RequestHeader("token") String token) {
		return RespUtil.baseResp(sysLoginService.userLogout(token));
	}

}
