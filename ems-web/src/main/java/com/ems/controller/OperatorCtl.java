/**
 * 
 */
package com.ems.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.ems.common.ServiceException;
import com.ems.config.annotation.CheckSign;
import com.ems.enums.RespCode;
import com.ems.service.ConfigEquRunService;
import com.ems.service.ConfigModeService;
import com.ems.utils.RespUtil;

/**
 * 运行管理：模式设置&查询、日常管理&查询
 * 
 * @author chenzhao @date Oct 18, 2019
 */
@RequestMapping("operation")
@RestController
public class OperatorCtl {

	@Autowired
	ConfigModeService configModeService;

	@Autowired
	ConfigEquRunService configEquRunService;
	
	/**
	 * 大屏监控、日常管理查询
	 * 
	 * @author chenzhao @date Oct 17, 2019
	 * @param version
	 * @return
	 */
	@RequestMapping("status")
	@CheckSign
	public Object monitorDashboard(@RequestHeader("version") String version) {
		if ("v1".equals(version)) {
			return RespUtil.dataResp(configEquRunService.dashboardV1());
		} else {
			throw new ServiceException(RespCode.REQ_VERSION_ERROR);
		}
	}

	/**
	 * 模式设置-查询
	 * 
	 * @author chenzhao @date Oct 18, 2019
	 * @param version
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "mode", method = RequestMethod.GET)
	@CheckSign
	public Object modeGet(@RequestHeader("version") String version) {
		if ("v1".equals(version)) {
			Map result = configModeService.modeListV1();
			return RespUtil.dataResp(result);
		} else {
			throw new ServiceException(RespCode.REQ_VERSION_ERROR);
		}

	}

	/**
	 * 模式设置-设置
	 * 
	 * @author chenzhao @date Oct 18, 2019
	 * @param version
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "mode", method = RequestMethod.POST)
	@CheckSign(isCheckLogin = true)
	public Object modePost(@RequestHeader("version") String version, @RequestBody String req) {
		if ("v1".equals(version)) {
			return RespUtil.baseResp(configModeService.modeInsertV1(JSONObject.parseObject(req)));
		} else {
			throw new ServiceException(RespCode.REQ_VERSION_ERROR);
		}

	}

	/**
	 * 日常管理-设置（各类型设备控制，多设备方面需求模糊）
	 * @author chenzhao @date Oct 18, 2019
	 * @param version
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "", method = RequestMethod.POST)
	@CheckSign(isCheckLogin = true)
	public Object operator(@RequestHeader("version") String version, @RequestBody String req) {
		if ("v1".equals(version)) {
			return RespUtil.baseResp(configEquRunService.operatorV1(JSONObject.parseObject(req)));
		} else {
			throw new ServiceException(RespCode.REQ_VERSION_ERROR);
		}

	}
}
