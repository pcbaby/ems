package com.ems.constants;

/**
 * redis键-常量类（防止redis键冲突）
 * @author ack @date Oct 19, 2019
 *
 */
public class RedisCons {
	
	/**
	 * 登录key
	 */
	public static final String loginToken="loginToken_";
	/**
	 * mock设备数据累计数
	 */
	public static final String mockEquCount="mockEquCount_";

}
