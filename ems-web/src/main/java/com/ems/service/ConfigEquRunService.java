/**
 * 
 */
package com.ems.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ems.common.ServiceException;
import com.ems.entity.ConfigEquRun;
import com.ems.enums.EquTypeEnum;
import com.ems.enums.ModeTypeEnum;
import com.ems.enums.RespCode;
import com.ems.mapper.ConfigEquRunMapper;

/**
 * 设备-配置数据+当前运行数据
 * 
 * @author chenzhao @date Oct 17, 2019
 */
@Service
public class ConfigEquRunService {

	@Autowired
	ConfigEquRunMapper configEquRunMapper;

	@SuppressWarnings("rawtypes")
	public Map dashboardV1() {
		HashMap<String, Object> resp = new HashMap<String, Object>();
		resp.put("mode", ModeTypeEnum.offline.getCode());
		resp.put(EquTypeEnum.cityGrid.getCode(), configEquRunMapper.selectCtiy());
		resp.put(EquTypeEnum.storedEnergy.getCode(), configEquRunMapper.selectStored());
		resp.put(EquTypeEnum.PV.getCode(), configEquRunMapper.selectPv());
		resp.put(EquTypeEnum.highPower.getCode(), configEquRunMapper.selectHighpower());
		resp.put(EquTypeEnum.V2L.getCode(), configEquRunMapper.selectV2L());
		resp.put(EquTypeEnum.DC.getCode(), configEquRunMapper.selectDc());
		resp.put(EquTypeEnum.AC.getCode(), configEquRunMapper.selectAc());
		resp.put(EquTypeEnum.normalLoad.getCode(), configEquRunMapper.selectNormalLoad());
		return resp;
	}

	public Map selectStored4single() {
		HashMap<String, Object> map = (HashMap<String, Object>) configEquRunMapper.selectStored4single();
		map.put("storedSingleDevice", JSONArray.parseArray(map.get("storedSingleDevice") + ""));
		return map;
	}

	/**
	 * @author chenzhao @date Oct 18, 2019
	 * @param req
	 * @return
	 */
	public boolean operatorV1(JSONObject req) {
		Integer switchStatus = req.getInteger("switchStatus");
		String type = req.getString("type");
		if (StringUtils.isEmpty(type)) {
			throw new ServiceException(RespCode.PARAM_INCOMPLETE, "type");
		}
		if (switchStatus == null) {
			throw new ServiceException(RespCode.PARAM_INCOMPLETE, "switchStatus");
		}

		if (type.equals(EquTypeEnum.cityGrid.getCode())) {
			// #向设备发送开关指令TODO

		} else if (type.equals(EquTypeEnum.storedEnergy.getCode())) {

		} else if (type.equals(EquTypeEnum.highPower.getCode())) {

		} else if (type.equals(EquTypeEnum.V2L.getCode())) {

		} else if (type.equals(EquTypeEnum.DC.getCode())) {

		} else {
			throw new ServiceException(RespCode.operator_equType_unexist);
		}

		if (1 == 2) {// 判断指令发送是否成功 TODO
			throw new ServiceException(RespCode.operator_control_error);
		}
		// #指令发送成功，更新设备当前最新状态
		ConfigEquRun configEquRun = new ConfigEquRun();
		configEquRun.setEquId(null);// 待支持多设备
		configEquRun.setType(type);
		configEquRun.setSwitchStatus(switchStatus);
		int count = configEquRunMapper.update(configEquRun);
		if (count < 1) {// 更新失败说明没数据，说明没有初始化查询设备数据+配置
			throw new ServiceException(RespCode.operator_equ_unInit);
		}

		return true;
	}

}
