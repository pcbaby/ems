/**
 * 
 */
package com.ems.service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.ems.common.InvokeService;
import com.ems.common.ServiceException;
import com.ems.entity.ConfigMode;
import com.ems.enums.ModeTypeEnum;
import com.ems.enums.RespCode;
import com.ems.mapper.ConfigEquRunMapper;
import com.ems.mapper.ConfigModeMapper;

/**
 * 模式设置
 * 
 * @author chenzhao @date Oct 18, 2019
 */
@Service
public class ConfigModeService {

	private static Logger log = LoggerFactory.getLogger(InvokeService.class);

	@Autowired
	ConfigModeMapper configModeMapper;

	@Autowired
	ConfigEquRunMapper configEquRunMapper;

	@SuppressWarnings("rawtypes")
	public Map modeListV1() {
		HashMap<String, Object> resp = new HashMap<String, Object>();
		resp.put(ModeTypeEnum.peakShaving.getCode(),
				objectsToMap(configModeMapper.selectList4Type(ModeTypeEnum.peakShaving.getCode())));
		resp.put(ModeTypeEnum.HPC.getCode(),
				objectsToMap(configModeMapper.selectList4Type(ModeTypeEnum.HPC.getCode())));
		resp.put(ModeTypeEnum.offline.getCode(),
				objectsToMap(configModeMapper.selectList4Type(ModeTypeEnum.offline.getCode())));
		return resp;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public boolean modeInsertV1(JSONObject req) {
		JSONObject peakShaving = req.getJSONObject(ModeTypeEnum.peakShaving.getCode());
		JSONObject hpc = req.getJSONObject(ModeTypeEnum.HPC.getCode());
		JSONObject offline = req.getJSONObject(ModeTypeEnum.offline.getCode());
		if (peakShaving == null || hpc == null || offline == null) {
			throw new ServiceException(RespCode.PARAM_INCOMPLETE, "peakShaving,HPC,offline");
		}
		int count = 0;
		for (Iterator iterator = peakShaving.entrySet().iterator(); iterator.hasNext();) {
			Entry<String, Object> entry = (Entry<String, Object>) iterator.next();
			try {
				configModeMapper.insert(ModeTypeEnum.peakShaving.getCode(), entry.getKey(), entry.getValue() + "");
				count++;
			} catch (DuplicateKeyException e) {
				log.info("peakShaving模式已存在，更新入库，#key:{},#value:{}", entry.getKey(), entry.getValue());
				configModeMapper.update(ModeTypeEnum.peakShaving.getCode(), entry.getKey(), entry.getValue() + "");
				count++;
			}
		}
		for (Iterator iterator = hpc.entrySet().iterator(); iterator.hasNext();) {
			Entry<String, Object> entry = (Entry<String, Object>) iterator.next();
			try {
				configModeMapper.insert(ModeTypeEnum.HPC.getCode(), entry.getKey(), entry.getValue() + "");
				count++;
			} catch (DuplicateKeyException e) {
				log.info("HPC模式已存在，更新入库，#key:{},#value:{}", entry.getKey(), entry.getValue());
				configModeMapper.update(ModeTypeEnum.HPC.getCode(), entry.getKey(), entry.getValue() + "");
				count++;
			}
		}
		for (Iterator iterator = offline.entrySet().iterator(); iterator.hasNext();) {
			Entry<String, Object> entry = (Entry<String, Object>) iterator.next();
			try {
				configModeMapper.insert(ModeTypeEnum.offline.getCode(), entry.getKey(), entry.getValue() + "");
				count++;
			} catch (DuplicateKeyException e) {
				log.info("offline模式已存在，更新入库，#key:{},#value:{}", entry.getKey(), entry.getValue());
				configModeMapper.update(ModeTypeEnum.offline.getCode(), entry.getKey(), entry.getValue() + "");
				count++;
			}
		}
		if (count != peakShaving.size() + hpc.size() + offline.size()) {
			throw new ServiceException(RespCode.operator_save_error);
		}
		return true;
	}

	@SuppressWarnings("rawtypes")
	Map objectsToMap(List<ConfigMode> configModeList) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		for (ConfigMode configMode : configModeList) {
			map.put(configMode.getKey(), configMode.getValue());
		}
		return map;
	}

	public static void main(String[] args) {
		JSONObject json = JSONObject.parseObject("{'b1':'cityGrid','B1':'22','endTime':'66'}");
		json.put("STARTTIME", "sdfsdsdf");
		System.out.println(json.getJSONObject("sdf"));
		System.out.println(json);
	}
}
