/**
 * 
 */
package com.ems.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ems.entity.ConfigMode;

/**
 * 消峰填谷策略-分析服务
 * 
 * @author chenzhao @date Nov 11, 2019
 */
public class PeakValleyService {
	private static Logger log = LoggerFactory.getLogger(PeakValleyService.class);

	public static void valleyInterval(ConfigMode A0, ConfigMode A1, ConfigMode W1, ConfigMode W2, long storedSoc,
			long cityPower) {
		if (storedSoc > Long.parseLong(A1.getValue()) && storedSoc < Long.parseLong(A0.getValue())) {
			// 当储能（BMS的soc）高于A1低于A0，储能可充可放
			log.info("当储能（BMS的soc）高于A1低于A0，储能可充可放");
			// 保障常规负载，限制AC、DC桩接入，全部负载额定功率需小于等于W2
			if (cityPower < Long.parseLong(W1.getValue())) {
				log.info("市电低于W1时，优先调低PCS（ACDC的功率）供电功率直至0，再调整PCS充电功率直至W10，再调低光伏输出功率直至0；");
				// TODO 调用设备控制指令服务
			} else if (cityPower > Long.parseLong(W2.getValue())) {
				log.info("当市电高于W2时，优先调高光伏供电功率直至W8，继续调低PCS充电功率/调高PCS供电功率直至W9。");
				// TODO 调用设备控制指令服务
			} else if (cityPower >= Long.parseLong(W1.getValue()) && cityPower <= Long.parseLong(W2.getValue())) {
				log.info("市电功率供电区间为W1-W2，");

			}

		} else if (storedSoc < Long.parseLong(A1.getValue())) {
			log.info("当储能低于A1，调低储能供电功率直至0，储能只充不放");
			// TODO 保障常规负载，限制AC、DC桩接入，全部负载额定功率需小于等于W2
			if (cityPower < Long.parseLong(W1.getValue())) {
				log.info("市电低于W1时，优先调高PCS充电功率直至W10，再调低光伏输出功率直至0；");
				// TODO 调用设备控制指令服务
			} else if (cityPower > Long.parseLong(W2.getValue())) {
				log.info("当市电高于W2时，优先调高光伏输出功率直至W8，再调低PCS充电功率直至0；");
				// TODO 调用设备控制指令服务
			} else if (cityPower >= Long.parseLong(W1.getValue()) && cityPower <= Long.parseLong(W2.getValue())) {
				log.info("市电功率供电区间为W1-W2，");

			}
		} else if (storedSoc > Long.parseLong(A0.getValue())) {
			log.info("储能超过最大阈值A0，调低PCS充电功率直至0，储能只放不充");
			// TODO 保障常规负载，限制AC、DC桩接入，全部负载额定功率需小于等于W2+W9；
			if (cityPower < Long.parseLong(W1.getValue())) {
				log.info("市电低于W1时，调低PCS供电功率直至0，再调低光伏输出功率直至0；");
				// TODO 调用设备控制指令服务
			} else if (cityPower > Long.parseLong(W2.getValue())) {
				log.info("当市电高于W2时，优先调高光伏输出功率直至W8，再调高PCS供电功率至W9；");
				// TODO 调用设备控制指令服务
			} else if (cityPower >= Long.parseLong(W1.getValue()) && cityPower <= Long.parseLong(W2.getValue())) {
				log.info("市电功率供电区间为W1-W2，");

			}
		}
	}

	public static void peakInterval(ConfigMode A3, ConfigMode W1, ConfigMode W2, long storedSoc, long cityPower) {
		if (storedSoc > Long.parseLong(A3.getValue())) {
			// 当储能（BMS的soc）高于A1低于A0，储能可充可放
			log.info("当储能高于A3，储能只放不充");
			// 保障常规负载，限制AC、DC桩接入，使得全部负载额定功率需小于等于W2+W9；
			if (cityPower < Long.parseLong(W1.getValue())) {
				log.info("低于W1时，优先调低PCS供电功率直至0，再调低光伏输出功率直至0；");
				// TODO 调用设备控制指令服务
			} else if (cityPower < Long.parseLong(A3.getValue())) {
				log.info("当高于W2时，优先调高光伏供电功率直至W8，继续调高PCS供电功率直至W9。");
				// TODO 调用设备控制指令服务
			} else if (cityPower >= Long.parseLong(W1.getValue()) && cityPower <= Long.parseLong(W2.getValue())) {
				log.info("市电功率供电区间为W1-W2，");

			}

		} else if (storedSoc < Long.parseLong(A3.getValue())) {
			log.info("当储能低于A3，调低储能供电功率直至0，储能不充不放");
			// TODO 保障常规负载，限制AC、DC桩接入，全部负载额定功率需小于等于W2；
			if (cityPower < Long.parseLong(W1.getValue())) {
				log.info("低于W1时，调低光伏输出功率直至0；");
				// TODO 调用设备控制指令服务
			} else if (cityPower > Long.parseLong(W2.getValue())) {
				log.info("当高于W2时，优先调高光伏输出功率直至W8；");
				// TODO 调用设备控制指令服务
			} else if (cityPower >= Long.parseLong(W1.getValue()) && cityPower <= Long.parseLong(W2.getValue())) {
				log.info("市电功率供电区间为W1-W2，");

			}
		}
	}

	public static void normallInterval(ConfigMode A2, ConfigMode A3, ConfigMode W1, ConfigMode W2, long storedSoc,
			long cityPower) {
		if (storedSoc > Long.parseLong(A2.getValue())) {
			// 当储能（BMS的soc）高于A1低于A0，储能可充可放
			log.info("当储能高于A2，储能可充可放");
			// 保障常规负载，限制AC、DC桩接入，使得全部负载额定功率需小于等于W2+W9；
			if (cityPower < Long.parseLong(W1.getValue())) {
				log.info("低于W1时，优先调低PCS供电功率直至0，再调低光伏输出功率直至0；");
				// TODO 调用设备控制指令服务
			} else if (cityPower > Long.parseLong(W2.getValue())) {
				log.info("当高于W2时， 优先调高光伏供电功率直至W8，继续调高PCS供电功率直至W9。");
				// TODO 调用设备控制指令服务
			} else if (cityPower >= Long.parseLong(W1.getValue()) && cityPower <= Long.parseLong(W2.getValue())) {
				log.info("市电功率供电区间为W1-W2，");

			}

		} else if (storedSoc > Long.parseLong(A3.getValue()) && storedSoc < Long.parseLong(A2.getValue())) {
			log.info("当储能低于A2且高于A3，调低储能供电功率直至0（不充不放）");
			// TODO 保障常规负载，限制AC、DC桩接入，全部负载额定功率需小于等于W2；
			if (cityPower < Long.parseLong(W1.getValue())) {
				log.info("低于W1时，调低光伏输出功率直至0；");
				// TODO 调用设备控制指令服务
			} else if (cityPower > Long.parseLong(W2.getValue())) {
				log.info("当高于W2时， 优先调高光伏输出功率直至W8；");
				// TODO 调用设备控制指令服务
			} else if (cityPower >= Long.parseLong(W1.getValue()) && cityPower <= Long.parseLong(W2.getValue())) {
				log.info("市电功率供电区间为W1-W2，");

			}
		} else if (storedSoc < Long.parseLong(A3.getValue())) {
			log.info("当储能低于A3，储能只充不放");
			// TODO 保障常规负载，限制AC、DC桩接入，使得全部负载额定功率需小于等于W2；
			if (cityPower < Long.parseLong(W1.getValue())) {
				log.info("低于W1时，调高PCS充电功率直至W10，再调低光伏输出功率直至0；");
				// TODO 调用设备控制指令服务
			} else if (cityPower > Long.parseLong(W2.getValue())) {
				log.info("当高于W2时，优先调高光伏供电功率直至W8，继续调低PCS充电功率至0。");
				// TODO 调用设备控制指令服务
			} else if (cityPower >= Long.parseLong(W1.getValue()) && cityPower <= Long.parseLong(W2.getValue())) {
				log.info("市电功率供电区间为W1-W2，");

			}
		}
	}
}
