/**
 * 
 */
package com.ems.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.ems.common.ServiceException;
import com.ems.entity.EquPower;
import com.ems.enums.EquTypeEnum;
import com.ems.enums.RespCode;
import com.ems.mapper.EquPowerMapper;

/**
 * 
 * @author chenzhao @date Oct 17, 2019
 */
@Service
public class EquPowerService {

	@Autowired
	EquPowerMapper equPowerMapper;

	/**
	 * 查询各设备类型，指定时间区间的运行功率数据
	 * 
	 * @author chenzhao @date Oct 17, 2019
	 * @param type
	 * @param startTime
	 * @param endTime
	 * @return
	 * @throws Exception
	 */
	public List<EquPower> monitorListDataV1(String type, long startTime, long endTime) throws Exception {
		if (StringUtils.isEmpty(type)) {
			throw new ServiceException(RespCode.PARAM_INCOMPLETE, "type");
		}
		if (StringUtils.isEmpty(startTime)) {
			throw new ServiceException(RespCode.PARAM_INCOMPLETE, "startTime");
		}
		if (StringUtils.isEmpty(endTime)) {
			throw new ServiceException(RespCode.PARAM_INCOMPLETE, "endTime");
		}
		if (endTime - startTime > 24 * 60 * 60 * 1000) {//起止时间间隔不能超过24小时
			throw new ServiceException(RespCode.monitor_timeinterval_out);
		}
		if (type.equals(EquTypeEnum.cityGrid.getCode())) {
			return equPowerMapper.selectCtiy(startTime, endTime);
		} else if (type.equals(EquTypeEnum.storedEnergy.getCode())) {
			return equPowerMapper.selectStored(startTime, endTime);
		} else if (type.equals(EquTypeEnum.PV.getCode())) {
			return equPowerMapper.selectPv(startTime, endTime);
		} else if (type.equals(EquTypeEnum.highPower.getCode())) {
			return equPowerMapper.selectHigntpower(startTime, endTime);
		} else if (type.equals(EquTypeEnum.DC.getCode())) {
			return equPowerMapper.selectDc(startTime, endTime);
		} else if (type.equals(EquTypeEnum.AC.getCode())) {
			return equPowerMapper.selectAc(startTime, endTime);
		} else {
			throw new ServiceException(RespCode.PARAM_ILLEGAL,
					"type合法值:cityGrid,storedEnergy,PV,highPower,DC,AC,general");
		}
	}

}
