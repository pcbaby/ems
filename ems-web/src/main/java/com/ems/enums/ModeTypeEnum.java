package com.ems.enums;

/**
 * 模式类型
 * 
 * @author chenzhao @date Oct 17, 2019
 */
public enum ModeTypeEnum {
	/**
	 * "peakShaving", "削峰填谷模式"
	 */
	peakShaving("peakShaving", "削峰填谷模式"),
	/**
	 * "HPC", "HPC保障模式"
	 */
	HPC("HPC", "HPC保障模式"),
	/**
	 * "offline", "离线模式"
	 */
	offline("offline", "离线模式");

	private String code;
	private String msg;

	ModeTypeEnum(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public String getCode() {
		return code;
	}

	public String getMsg() {
		return msg;
	}

}
