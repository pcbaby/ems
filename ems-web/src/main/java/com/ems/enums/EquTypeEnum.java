package com.ems.enums;

/**
 * 设备类型
 * 
 * @author chenzhao @date Oct 17, 2019
 */
public enum EquTypeEnum {
	/**
	 * "cityGrid", "市电"
	 */
	cityGrid("cityGrid", "市电"),
	/**
	 * "storedEnergy", "储能"
	 */
	storedEnergy("storedEnergy", "储能"),
	/**
	 * "PV", "光伏"
	 */
	PV("PV", "光伏"),
	/**
	 * "V2L", "V2L"
	 */
	V2L("V2L", "V2L"),
	/**
	 * "DC", "DC桩"
	 */
	DC("DC", "DC桩"),
	/**
	 * "AC", "AC桩"
	 */
	AC("AC", "AC桩"),
	/**
	 * ACDC
	 */
	ACDC("ACDC", "ACDC"),
	/**
	 * "normalLoad", "常规负载"
	 */
	normalLoad("normalLoad", "常规负载"),
	/**
	 * "highPower", "大功率设备"normalLoad
	 */
	highPower("highPower", "大功率设备");

	private String code;
	private String msg;

	EquTypeEnum(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public String getCode() {
		return code;
	}

	public String getMsg() {
		return msg;
	}

}
