/**
 * 
 */
package com.ems.entity;

/**
 * @author chenzhao @date Oct 17, 2019
 */
public class ConfigMode {

	private long id;
	private String type;
	private String key;
	private String value;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
