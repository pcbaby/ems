/**
 * 
 */
package com.ems.entity;

/**
 * @author chenzhao @date Oct 17, 2019
 */
public class ConfigEquRun {

	private Long id;
	private String type;
	private String equId;
	private Integer switchStatus;
	private Long maxPower;
	private long realTimePower;
	private long realTimePowerUseless;
	private long realTimePowerView;
	private long realTimeEnergy;
	private long realTimeEnergyForward;
	private long realTimeEnergyReverse;
	private long realTimeCurrent0;
	private long realTimeCurrent1;
	private long realTimeCurrent2;
	private long realTimeVoltage0;
	private long realTimeVoltage1;
	private long realTimeVoltage2;

	// #特定设备才有的属性
	private String storedRealTimesoc;
	private String storedRealTimeCapacity;
	private String storedChargeStatus;
	private String storedMaxChargePower;
	private String storedMaxDischargePower;
	private String storedSoc;
	private String storedSoh;

	private String storedProvider;
	private String storedDeviceType;
	private long storedMaxEnergy;
	private String storedInstallTime;
	private String storedInstallPosition;
	private long storedGridVoltage;
	private long storedGridCurrent;
	private long storedEpsVoltage;
	private long storedEpsCurrent;
	private long storedBatteryVoltage;
	private long storedBatteryCurrent;
	private long storedDailyChargeEnergy;
	private long storedDailyDischargeEnergy;
	private long storedTotalChargeEnergy;
	private long storedTotalDischargeEnergy;
	private long storedDailyEpsDischargeEnergy;
	private long storedTotalEpsDischargeEnergy;
	private String storedSingleDevice;

	private String pvCapacity;
	private String citygridSetPowerLow;
	private String citygridSetPowerHigh;

	/**
	 * 
	 */
	public ConfigEquRun() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getEquId() {
		return equId;
	}

	public void setEquId(String equId) {
		this.equId = equId;
	}

	public Integer getSwitchStatus() {
		return switchStatus;
	}

	public void setSwitchStatus(Integer switchStatus) {
		this.switchStatus = switchStatus;
	}

	public Long getMaxPower() {
		return maxPower;
	}

	public void setMaxPower(Long maxPower) {
		this.maxPower = maxPower;
	}

	public long getRealTimePower() {
		return realTimePower;
	}

	public void setRealTimePower(long realTimePower) {
		this.realTimePower = realTimePower;
	}

	public long getRealTimePowerUseless() {
		return realTimePowerUseless;
	}

	public void setRealTimePowerUseless(long realTimePowerUseless) {
		this.realTimePowerUseless = realTimePowerUseless;
	}

	public long getRealTimePowerView() {
		return realTimePowerView;
	}

	public void setRealTimePowerView(long realTimePowerView) {
		this.realTimePowerView = realTimePowerView;
	}

	public long getRealTimeEnergy() {
		return realTimeEnergy;
	}

	public void setRealTimeEnergy(long realTimeEnergy) {
		this.realTimeEnergy = realTimeEnergy;
	}

	public long getRealTimeEnergyForward() {
		return realTimeEnergyForward;
	}

	public void setRealTimeEnergyForward(long realTimeEnergyForward) {
		this.realTimeEnergyForward = realTimeEnergyForward;
	}

	public long getRealTimeEnergyReverse() {
		return realTimeEnergyReverse;
	}

	public void setRealTimeEnergyReverse(long realTimeEnergyReverse) {
		this.realTimeEnergyReverse = realTimeEnergyReverse;
	}

	public long getRealTimeCurrent0() {
		return realTimeCurrent0;
	}

	public void setRealTimeCurrent0(long realTimeCurrent0) {
		this.realTimeCurrent0 = realTimeCurrent0;
	}

	public long getRealTimeCurrent1() {
		return realTimeCurrent1;
	}

	public void setRealTimeCurrent1(long realTimeCurrent1) {
		this.realTimeCurrent1 = realTimeCurrent1;
	}

	public long getRealTimeCurrent2() {
		return realTimeCurrent2;
	}

	public void setRealTimeCurrent2(long realTimeCurrent2) {
		this.realTimeCurrent2 = realTimeCurrent2;
	}

	public long getRealTimeVoltage0() {
		return realTimeVoltage0;
	}

	public void setRealTimeVoltage0(long realTimeVoltage0) {
		this.realTimeVoltage0 = realTimeVoltage0;
	}

	public long getRealTimeVoltage1() {
		return realTimeVoltage1;
	}

	public void setRealTimeVoltage1(long realTimeVoltage1) {
		this.realTimeVoltage1 = realTimeVoltage1;
	}

	public long getRealTimeVoltage2() {
		return realTimeVoltage2;
	}

	public void setRealTimeVoltage2(long realTimeVoltage2) {
		this.realTimeVoltage2 = realTimeVoltage2;
	}

	public String getStoredRealTimesoc() {
		return storedRealTimesoc;
	}

	public void setStoredRealTimesoc(String storedRealTimesoc) {
		this.storedRealTimesoc = storedRealTimesoc;
	}

	public String getStoredRealTimeCapacity() {
		return storedRealTimeCapacity;
	}

	public void setStoredRealTimeCapacity(String storedRealTimeCapacity) {
		this.storedRealTimeCapacity = storedRealTimeCapacity;
	}

	public String getStoredChargeStatus() {
		return storedChargeStatus;
	}

	public void setStoredChargeStatus(String storedChargeStatus) {
		this.storedChargeStatus = storedChargeStatus;
	}

	public String getStoredMaxChargePower() {
		return storedMaxChargePower;
	}

	public void setStoredMaxChargePower(String storedMaxChargePower) {
		this.storedMaxChargePower = storedMaxChargePower;
	}

	public String getStoredMaxDischargePower() {
		return storedMaxDischargePower;
	}

	public void setStoredMaxDischargePower(String storedMaxDischargePower) {
		this.storedMaxDischargePower = storedMaxDischargePower;
	}

	public String getStoredSoc() {
		return storedSoc;
	}

	public void setStoredSoc(String storedSoc) {
		this.storedSoc = storedSoc;
	}

	public String getStoredSoh() {
		return storedSoh;
	}

	public void setStoredSoh(String storedSoh) {
		this.storedSoh = storedSoh;
	}

	public String getStoredProvider() {
		return storedProvider;
	}

	public void setStoredProvider(String storedProvider) {
		this.storedProvider = storedProvider;
	}

	public String getStoredDeviceType() {
		return storedDeviceType;
	}

	public void setStoredDeviceType(String storedDeviceType) {
		this.storedDeviceType = storedDeviceType;
	}

	public long getStoredMaxEnergy() {
		return storedMaxEnergy;
	}

	public void setStoredMaxEnergy(long storedMaxEnergy) {
		this.storedMaxEnergy = storedMaxEnergy;
	}

	public String getStoredInstallTime() {
		return storedInstallTime;
	}

	public void setStoredInstallTime(String storedInstallTime) {
		this.storedInstallTime = storedInstallTime;
	}

	public String getStoredInstallPosition() {
		return storedInstallPosition;
	}

	public void setStoredInstallPosition(String storedInstallPosition) {
		this.storedInstallPosition = storedInstallPosition;
	}

	public long getStoredGridVoltage() {
		return storedGridVoltage;
	}

	public void setStoredGridVoltage(long storedGridVoltage) {
		this.storedGridVoltage = storedGridVoltage;
	}

	public long getStoredGridCurrent() {
		return storedGridCurrent;
	}

	public void setStoredGridCurrent(long storedGridCurrent) {
		this.storedGridCurrent = storedGridCurrent;
	}

	public long getStoredEpsVoltage() {
		return storedEpsVoltage;
	}

	public void setStoredEpsVoltage(long storedEpsVoltage) {
		this.storedEpsVoltage = storedEpsVoltage;
	}

	public long getStoredEpsCurrent() {
		return storedEpsCurrent;
	}

	public void setStoredEpsCurrent(long storedEpsCurrent) {
		this.storedEpsCurrent = storedEpsCurrent;
	}

	public long getStoredBatteryVoltage() {
		return storedBatteryVoltage;
	}

	public void setStoredBatteryVoltage(long storedBatteryVoltage) {
		this.storedBatteryVoltage = storedBatteryVoltage;
	}

	public long getStoredBatteryCurrent() {
		return storedBatteryCurrent;
	}

	public void setStoredBatteryCurrent(long storedBatteryCurrent) {
		this.storedBatteryCurrent = storedBatteryCurrent;
	}

	public long getStoredDailyChargeEnergy() {
		return storedDailyChargeEnergy;
	}

	public void setStoredDailyChargeEnergy(long storedDailyChargeEnergy) {
		this.storedDailyChargeEnergy = storedDailyChargeEnergy;
	}

	public long getStoredDailyDischargeEnergy() {
		return storedDailyDischargeEnergy;
	}

	public void setStoredDailyDischargeEnergy(long storedDailyDischargeEnergy) {
		this.storedDailyDischargeEnergy = storedDailyDischargeEnergy;
	}

	public long getStoredTotalChargeEnergy() {
		return storedTotalChargeEnergy;
	}

	public void setStoredTotalChargeEnergy(long storedTotalChargeEnergy) {
		this.storedTotalChargeEnergy = storedTotalChargeEnergy;
	}

	public long getStoredTotalDischargeEnergy() {
		return storedTotalDischargeEnergy;
	}

	public void setStoredTotalDischargeEnergy(long storedTotalDischargeEnergy) {
		this.storedTotalDischargeEnergy = storedTotalDischargeEnergy;
	}

	public long getStoredDailyEpsDischargeEnergy() {
		return storedDailyEpsDischargeEnergy;
	}

	public void setStoredDailyEpsDischargeEnergy(long storedDailyEpsDischargeEnergy) {
		this.storedDailyEpsDischargeEnergy = storedDailyEpsDischargeEnergy;
	}

	public long getStoredTotalEpsDischargeEnergy() {
		return storedTotalEpsDischargeEnergy;
	}

	public void setStoredTotalEpsDischargeEnergy(long storedTotalEpsDischargeEnergy) {
		this.storedTotalEpsDischargeEnergy = storedTotalEpsDischargeEnergy;
	}

	public String getStoredSingleDevice() {
		return storedSingleDevice;
	}

	public void setStoredSingleDevice(String storedSingleDevice) {
		this.storedSingleDevice = storedSingleDevice;
	}

	public String getPvCapacity() {
		return pvCapacity;
	}

	public void setPvCapacity(String pvCapacity) {
		this.pvCapacity = pvCapacity;
	}

	public String getCitygridSetPowerLow() {
		return citygridSetPowerLow;
	}

	public void setCitygridSetPowerLow(String citygridSetPowerLow) {
		this.citygridSetPowerLow = citygridSetPowerLow;
	}

	public String getCitygridSetPowerHigh() {
		return citygridSetPowerHigh;
	}

	public void setCitygridSetPowerHigh(String citygridSetPowerHigh) {
		this.citygridSetPowerHigh = citygridSetPowerHigh;
	}

}
