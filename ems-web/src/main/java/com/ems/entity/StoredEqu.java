/**
 * 
 */
package com.ems.entity;

/**
 * 储能设备
 * 
 * @author chenzhao @date Oct 25, 2019
 */
public class StoredEqu extends EquPower {

	private String baseStatus;
	private long totalVoltage;
	private long temperature;
	private long soc;
	private long cycleTime;
	private long maxChargeVoltageofpile;
	private long maxChargeCurrent;
	private long minDischargeVoltageofpile;
	private long maxDischargeCurrent;
	private long soh;
	private long remainCapacity;
	private long chargeCapactityofpile;
	private long dischargeCapacityofpile;
	private long dailyAccumulateChargeCapactiy;
	private long dailyAccumulateDischargeCapactiy;
	private long historyAccumulateChargeCapacity;
	private long historyAccumulateDischargeCapacity;
	private long forceChargeRequestMark;
	private long balanceChargeRequestMark;
	private String errorCodes1;
	private String errorCodes2;
	private long moduleNumberInSeries;
	private long cellNumberInSeries;
	private long chargeForbiddenMark;
	private long dischargeForbiddenMark;
	private String pileVoltage;
	private String pileTemperature;
	private String pileCurrent;

	public String getBaseStatus() {
		return baseStatus;
	}

	public void setBaseStatus(String baseStatus) {
		this.baseStatus = baseStatus;
	}

	public long getTotalVoltage() {
		return totalVoltage;
	}

	public void setTotalVoltage(long totalVoltage) {
		this.totalVoltage = totalVoltage;
	}

	public long getTemperature() {
		return temperature;
	}

	public void setTemperature(long temperature) {
		this.temperature = temperature;
	}

	public long getSoc() {
		return soc;
	}

	public void setSoc(long soc) {
		this.soc = soc;
	}

	public long getCycleTime() {
		return cycleTime;
	}

	public void setCycleTime(long cycleTime) {
		this.cycleTime = cycleTime;
	}

	public long getMaxChargeVoltageofpile() {
		return maxChargeVoltageofpile;
	}

	public void setMaxChargeVoltageofpile(long maxChargeVoltageofpile) {
		this.maxChargeVoltageofpile = maxChargeVoltageofpile;
	}

	public long getMaxChargeCurrent() {
		return maxChargeCurrent;
	}

	public void setMaxChargeCurrent(long maxChargeCurrent) {
		this.maxChargeCurrent = maxChargeCurrent;
	}

	public long getMinDischargeVoltageofpile() {
		return minDischargeVoltageofpile;
	}

	public void setMinDischargeVoltageofpile(long minDischargeVoltageofpile) {
		this.minDischargeVoltageofpile = minDischargeVoltageofpile;
	}

	public long getMaxDischargeCurrent() {
		return maxDischargeCurrent;
	}

	public void setMaxDischargeCurrent(long maxDischargeCurrent) {
		this.maxDischargeCurrent = maxDischargeCurrent;
	}

	public long getSoh() {
		return soh;
	}

	public void setSoh(long soh) {
		this.soh = soh;
	}

	public long getRemainCapacity() {
		return remainCapacity;
	}

	public void setRemainCapacity(long remainCapacity) {
		this.remainCapacity = remainCapacity;
	}

	public long getChargeCapactityofpile() {
		return chargeCapactityofpile;
	}

	public void setChargeCapactityofpile(long chargeCapactityofpile) {
		this.chargeCapactityofpile = chargeCapactityofpile;
	}

	public long getDischargeCapacityofpile() {
		return dischargeCapacityofpile;
	}

	public void setDischargeCapacityofpile(long dischargeCapacityofpile) {
		this.dischargeCapacityofpile = dischargeCapacityofpile;
	}

	public long getDailyAccumulateChargeCapactiy() {
		return dailyAccumulateChargeCapactiy;
	}

	public void setDailyAccumulateChargeCapactiy(long dailyAccumulateChargeCapactiy) {
		this.dailyAccumulateChargeCapactiy = dailyAccumulateChargeCapactiy;
	}

	public long getDailyAccumulateDischargeCapactiy() {
		return dailyAccumulateDischargeCapactiy;
	}

	public void setDailyAccumulateDischargeCapactiy(long dailyAccumulateDischargeCapactiy) {
		this.dailyAccumulateDischargeCapactiy = dailyAccumulateDischargeCapactiy;
	}

	public long getHistoryAccumulateChargeCapacity() {
		return historyAccumulateChargeCapacity;
	}

	public void setHistoryAccumulateChargeCapacity(long historyAccumulateChargeCapacity) {
		this.historyAccumulateChargeCapacity = historyAccumulateChargeCapacity;
	}

	public long getHistoryAccumulateDischargeCapacity() {
		return historyAccumulateDischargeCapacity;
	}

	public void setHistoryAccumulateDischargeCapacity(long historyAccumulateDischargeCapacity) {
		this.historyAccumulateDischargeCapacity = historyAccumulateDischargeCapacity;
	}

	public long getForceChargeRequestMark() {
		return forceChargeRequestMark;
	}

	public void setForceChargeRequestMark(long forceChargeRequestMark) {
		this.forceChargeRequestMark = forceChargeRequestMark;
	}

	public long getBalanceChargeRequestMark() {
		return balanceChargeRequestMark;
	}

	public void setBalanceChargeRequestMark(long balanceChargeRequestMark) {
		this.balanceChargeRequestMark = balanceChargeRequestMark;
	}

	public String getErrorCodes1() {
		return errorCodes1;
	}

	public void setErrorCodes1(String errorCodes1) {
		this.errorCodes1 = errorCodes1;
	}

	public String getErrorCodes2() {
		return errorCodes2;
	}

	public void setErrorCodes2(String errorCodes2) {
		this.errorCodes2 = errorCodes2;
	}

	public long getModuleNumberInSeries() {
		return moduleNumberInSeries;
	}

	public void setModuleNumberInSeries(long moduleNumberInSeries) {
		this.moduleNumberInSeries = moduleNumberInSeries;
	}

	public long getCellNumberInSeries() {
		return cellNumberInSeries;
	}

	public void setCellNumberInSeries(long cellNumberInSeries) {
		this.cellNumberInSeries = cellNumberInSeries;
	}

	public long getChargeForbiddenMark() {
		return chargeForbiddenMark;
	}

	public void setChargeForbiddenMark(long chargeForbiddenMark) {
		this.chargeForbiddenMark = chargeForbiddenMark;
	}

	public long getDischargeForbiddenMark() {
		return dischargeForbiddenMark;
	}

	public void setDischargeForbiddenMark(long dischargeForbiddenMark) {
		this.dischargeForbiddenMark = dischargeForbiddenMark;
	}

	public String getPileVoltage() {
		return pileVoltage;
	}

	public void setPileVoltage(String pileVoltage) {
		this.pileVoltage = pileVoltage;
	}

	public String getPileTemperature() {
		return pileTemperature;
	}

	public void setPileTemperature(String pileTemperature) {
		this.pileTemperature = pileTemperature;
	}

	public String getPileCurrent() {
		return pileCurrent;
	}

	public void setPileCurrent(String pileCurrent) {
		this.pileCurrent = pileCurrent;
	}

}
