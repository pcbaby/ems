/**
 * 
 */
package com.ems.entity;

/**
 * 所有设备类型采集的公共属性
 * @author chenzhao @date Oct 17, 2019
 */
public class EquPower {

	private long id;
	private String equId;
	private long power;
	private long realTimeCurrent;
	private long realTimeVoltage;
	private String collectTime;
	private String collectTimestamp;

	private int chargeStatus;

	public EquPower() {
		super();
	}

	public EquPower(String equId, long power, String collectTimestamp) {
		super();
		this.equId = equId;
		this.power = power;
		this.collectTimestamp = collectTimestamp;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getEquId() {
		return equId;
	}

	public void setEquId(String equId) {
		this.equId = equId;
	}

	public long getPower() {
		return power;
	}

	public void setPower(long power) {
		this.power = power;
	}

	public long getRealTimeCurrent() {
		return realTimeCurrent;
	}

	public void setRealTimeCurrent(long realTimeCurrent) {
		this.realTimeCurrent = realTimeCurrent;
	}

	public long getRealTimeVoltage() {
		return realTimeVoltage;
	}

	public void setRealTimeVoltage(long realTimeVoltage) {
		this.realTimeVoltage = realTimeVoltage;
	}

	public String getCollectTime() {
		return collectTime;
	}

	public void setCollectTime(String collectTime) {
		this.collectTime = collectTime;
	}

	public String getCollectTimestamp() {
		return collectTimestamp;
	}

	public void setCollectTimestamp(String collectTimestamp) {
		this.collectTimestamp = collectTimestamp;
	}

	public int getChargeStatus() {
		return chargeStatus;
	}

	public void setChargeStatus(int chargeStatus) {
		this.chargeStatus = chargeStatus;
	}

}
