/*
Navicat MySQL Data Transfer

Source Server         : 49.4.10.6_EMS
Source Server Version : 50728
Source Host           : 49.4.10.6:3306
Source Database       : ems

Target Server Type    : MYSQL
Target Server Version : 50728
File Encoding         : 65001

Date: 2019-10-23 16:42:38
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alarm_log
-- ----------------------------
DROP TABLE IF EXISTS `alarm_log`;
CREATE TABLE `alarm_log` (
  `logid` int(11) NOT NULL AUTO_INCREMENT COMMENT '日志表主键',
  `device_type` varchar(255) DEFAULT NULL COMMENT '设备类型',
  `device_num` varchar(255) DEFAULT NULL COMMENT '设备编号',
  `alarm_type` varchar(255) DEFAULT NULL COMMENT '报警类型',
  `alarm_time` date DEFAULT NULL COMMENT '报警时间',
  `device_status` varchar(255) DEFAULT NULL COMMENT '设备状态',
  PRIMARY KEY (`logid`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of alarm_log
-- ----------------------------
INSERT INTO `alarm_log` VALUES ('1', '设备类型A', '123456789123456', '报警类型A', '2019-10-13', '故障');
INSERT INTO `alarm_log` VALUES ('2', '设备类型A', '123456789123456', '报警类型A', '2019-10-08', '故障');
INSERT INTO `alarm_log` VALUES ('3', '设备类型A', '123456789123456', '报警类型A', '2019-10-09', '故障');
INSERT INTO `alarm_log` VALUES ('4', '设备类型A', '12321477575', '报警类型B', '2019-10-10', '故障');
INSERT INTO `alarm_log` VALUES ('5', '设备类型B', '1234567890', '报警类型B', '2019-10-11', '故障');

-- ----------------------------
-- Table structure for config_equ_run
-- ----------------------------
DROP TABLE IF EXISTS `config_equ_run`;
CREATE TABLE `config_equ_run` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) NOT NULL COMMENT '设备类型(cityGrid,storedEnergy,PV,highPower,DC,AC,general)',
  `equ_id` varchar(25) NOT NULL COMMENT '设备唯一标识',
  `switch_status` tinyint(5) NOT NULL COMMENT '开关状态，"0":关，"1":开',
  `max_power` int(11) NOT NULL COMMENT '最大供电功率（额定功率），单位:0.0001kvar',
  `real_time_power` int(11) DEFAULT NULL COMMENT '实时功率，单位:0.0001kvar',
  `real_time_current` int(11) DEFAULT NULL COMMENT '实时电流，单位：0.01A',
  `real_time_voltage` int(11) DEFAULT NULL COMMENT '实时电压，单位：0.1V',
  `stored_real_time_soc` int(11) DEFAULT NULL COMMENT '储能-实时SOC，单位：1%',
  `stored_real_time_capacity` int(11) DEFAULT NULL,
  `stored_charge_status` tinyint(5) DEFAULT NULL COMMENT '储能-充放电状态(0:充电；1:放电)',
  `stored_max_charge_power` int(11) DEFAULT NULL,
  `stored_max_discharge_power` int(11) DEFAULT NULL,
  `stored_soc` varchar(25) DEFAULT NULL COMMENT '储能-额定SOC，单位：1%',
  `stored_soh` varchar(25) DEFAULT NULL COMMENT '储能-额定SOH，单位：1%',
  `stored_provider` varchar(11) DEFAULT NULL,
  `stored_device_type` varchar(25) DEFAULT NULL,
  `stored_max_engine` int(255) DEFAULT NULL,
  `stored_install_time` varchar(25) DEFAULT NULL,
  `stored_install_position` varchar(50) DEFAULT NULL,
  `stored_grid_voltage` int(11) DEFAULT NULL,
  `stored_grid_current` int(11) DEFAULT NULL,
  `stored_eps_current` int(11) DEFAULT NULL,
  `stored_battery_voltage` int(11) DEFAULT NULL,
  `stored_battery_current` int(11) DEFAULT NULL,
  `stored_daily_charge_power` int(11) DEFAULT NULL,
  `stored_daily_discharge_power` int(11) DEFAULT NULL,
  `stored_total_charge_power` int(11) DEFAULT NULL,
  `stored_total_discharge_power` int(11) DEFAULT NULL,
  `stored_daily_eps_discharge_power` int(11) DEFAULT NULL,
  `stored_total_eps_discharge_power` int(11) DEFAULT NULL,
  `stored_single_device` varchar(500) DEFAULT NULL,
  `pv_capacity` int(11) DEFAULT NULL COMMENT '光伏-装机容量，单位：Wh(暂定)',
  `pv_real_time_current0` int(11) DEFAULT NULL COMMENT '光伏-实时发电电流（A相），单位：0.01A',
  `pv_real_time_current1` int(11) DEFAULT NULL COMMENT '光伏-实时发电电流（B相），单位：0.01A',
  `pv_real_time_current2` int(11) DEFAULT NULL COMMENT '光伏-实时发电电流（C相），单位：0.01A',
  `pv_real_time_voltage0` int(11) DEFAULT NULL COMMENT '光伏-实时发电电压（A相）单位：0.1V',
  `pv_real_time_voltage1` int(11) DEFAULT NULL COMMENT '光伏-实时发电电压（B相）单位：0.1V',
  `pv_real_time_voltage2` int(11) DEFAULT NULL COMMENT '光伏-实时发电电压（C相）单位：0.1V',
  `citygrid_set_power_low` int(11) DEFAULT NULL COMMENT '市网-设定功率（低），单位:0.0001kvar',
  `citygrid_set_power_high` int(11) DEFAULT NULL COMMENT '市网-设定功率（高），单位:0.0001kvar',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_type_equId` (`type`,`equ_id`) USING BTREE COMMENT '每种设备类型下，一台设备只能有一条记录'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='设备-运行实时数据+配置数据(包含所有设备类型[市电、储能、光伏、DC桩、AC桩、大功率设备、常规负载])';

-- ----------------------------
-- Records of config_equ_run
-- ----------------------------
INSERT INTO `config_equ_run` VALUES ('1', 'cityGrid', '1', '1', '0', '0', '0', '0', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '1002', '6082');
INSERT INTO `config_equ_run` VALUES ('2', 'storedEnergy', '1', '0', '1007', '4343', '564', '345', '5552', '8039', '1', '1007', '6028', '30028', '30028', '阳关能源', '储能设备', '10000000', '20190909', '集装箱', '2200', '2000', '2200', '5000', '2200', '1000', '4000000', '500000', '300000', '410000', '350000', '[{\"voltage\": 22013,\"soc\": 42,\"temperature\": 591},{\"voltage\": 22013,\"soc\": 42,\"temperature\": 591}]', null, null, null, null, null, null, null, null, null);
INSERT INTO `config_equ_run` VALUES ('3', 'PV', '1', '1', '2222', '2222', '564', '345', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '5558', '111', '2222', '333', '22221', '2222', '22223', null, null);
INSERT INTO `config_equ_run` VALUES ('4', 'highPower', '1', '1', '8261', '34', '564', '345', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `config_equ_run` VALUES ('5', 'V2L', '1', '0', '12092', '5582', '11072', '5002', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `config_equ_run` VALUES ('6', 'normalLoad', '1', '1', '8092', '5552', '34', '34', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `config_equ_run` VALUES ('7', 'DC', '1', '0', '12092', '5582', '11072', '5002', null, null, null, null, null, '', '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `config_equ_run` VALUES ('8', 'AC', '1', '0', '1203', '5582', '11072', '5002', null, null, null, null, null, '', '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for config_mode
-- ----------------------------
DROP TABLE IF EXISTS `config_mode`;
CREATE TABLE `config_mode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL COMMENT '模式类型',
  `key` varchar(25) NOT NULL COMMENT '模式-属性key',
  `value` varchar(25) NOT NULL COMMENT '模式-属性值',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_type_key` (`type`,`key`) USING BTREE COMMENT '每种类型下key唯一'
) ENGINE=InnoDB AUTO_INCREMENT=370 DEFAULT CHARSET=utf8 COMMENT='模式配置表';

-- ----------------------------
-- Records of config_mode
-- ----------------------------
INSERT INTO `config_mode` VALUES ('1', 'peakShaving', 'W1', '1000');
INSERT INTO `config_mode` VALUES ('2', 'peakShaving', 'W2', '1000');
INSERT INTO `config_mode` VALUES ('3', 'peakShaving', 'A1', '1000');
INSERT INTO `config_mode` VALUES ('5', 'peakShaving', 'W3', '1000');
INSERT INTO `config_mode` VALUES ('6', 'peakShaving', 'W4', '1000');
INSERT INTO `config_mode` VALUES ('7', 'peakShaving', 'A2', '1000');
INSERT INTO `config_mode` VALUES ('9', 'peakShaving', 'W5', '1000');
INSERT INTO `config_mode` VALUES ('10', 'peakShaving', 'W6', '1000');
INSERT INTO `config_mode` VALUES ('11', 'peakShaving', 'A3', '1000');
INSERT INTO `config_mode` VALUES ('13', 'peakShaving', 'B1', '1000');
INSERT INTO `config_mode` VALUES ('14', 'peakShaving', 'B2', '1000');
INSERT INTO `config_mode` VALUES ('16', 'peakShaving', 'W7', '1000');
INSERT INTO `config_mode` VALUES ('17', 'peakShaving', 'W8', '1000');
INSERT INTO `config_mode` VALUES ('18', 'peakShaving', 'W9', '1000');
INSERT INTO `config_mode` VALUES ('19', 'peakShaving', 'W10', '1000');
INSERT INTO `config_mode` VALUES ('20', 'peakShaving', 'A0', '1000');
INSERT INTO `config_mode` VALUES ('21', 'HPC', 'W1', '2034');
INSERT INTO `config_mode` VALUES ('22', 'HPC', 'W2', '8045');
INSERT INTO `config_mode` VALUES ('23', 'HPC', 'A1', '8092');
INSERT INTO `config_mode` VALUES ('24', 'HPC', 'A2', '8092');
INSERT INTO `config_mode` VALUES ('25', 'HPC', 'B1', '1002');
INSERT INTO `config_mode` VALUES ('26', 'HPC', 'B2', '6082');
INSERT INTO `config_mode` VALUES ('28', 'HPC', 'W7', '6082');
INSERT INTO `config_mode` VALUES ('29', 'HPC', 'W8', '6082');
INSERT INTO `config_mode` VALUES ('30', 'HPC', 'W9', '6082');
INSERT INTO `config_mode` VALUES ('31', 'HPC', 'W10', '6082');
INSERT INTO `config_mode` VALUES ('32', 'HPC', 'A0', '6082');
INSERT INTO `config_mode` VALUES ('33', 'offline', 'A1', '2034');
INSERT INTO `config_mode` VALUES ('35', 'offline', 'W11', '8092');
INSERT INTO `config_mode` VALUES ('36', 'offline', 'A2', '8092');
INSERT INTO `config_mode` VALUES ('37', 'offline', 'B1', '1002');
INSERT INTO `config_mode` VALUES ('38', 'offline', 'B2', '6082');
INSERT INTO `config_mode` VALUES ('40', 'offline', 'W8', '6082');
INSERT INTO `config_mode` VALUES ('41', 'offline', 'W9', '6082');
INSERT INTO `config_mode` VALUES ('253', 'peakShaving', 'aa1', '1000');
INSERT INTO `config_mode` VALUES ('257', 'peakShaving', 'aa3', '1000');
INSERT INTO `config_mode` VALUES ('258', 'peakShaving', 'aa2', '1000');
INSERT INTO `config_mode` VALUES ('259', 'peakShaving', 'bb2', '1000');
INSERT INTO `config_mode` VALUES ('273', 'HPC', 'bb2', '6082');
INSERT INTO `config_mode` VALUES ('280', 'offline', 'aa1', '8045');
INSERT INTO `config_mode` VALUES ('285', 'offline', 'bb2', '6082');

-- ----------------------------
-- Table structure for ems_data
-- ----------------------------
DROP TABLE IF EXISTS `ems_data`;
CREATE TABLE `ems_data` (
  `did` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `high_power` decimal(10,2) DEFAULT NULL COMMENT '大功率用电',
  `v2l_elec` decimal(10,2) DEFAULT NULL COMMENT 'V2l累计用电',
  `dc_elec` decimal(10,2) DEFAULT NULL COMMENT 'DC桩累计用电',
  `ac_elec` decimal(10,2) DEFAULT NULL COMMENT 'AC桩累计用电',
  `vall_elec` decimal(10,2) DEFAULT NULL COMMENT '谷电累计充电',
  `ordi_tele` decimal(10,2) DEFAULT NULL COMMENT '平电累计充电',
  `peak_elec` decimal(10,2) DEFAULT NULL COMMENT '峰电累计充电',
  `total1` decimal(11,2) DEFAULT NULL COMMENT '储能累计充电',
  `vall_disc` decimal(10,2) DEFAULT NULL COMMENT '谷电累计放电',
  `ordi_disc` decimal(10,2) DEFAULT NULL COMMENT '平电累计放电',
  `peak_disc` decimal(10,2) DEFAULT NULL COMMENT '峰电累计放电',
  `total2` decimal(10,2) DEFAULT NULL COMMENT '储能累计放电',
  `vall_gen` decimal(10,2) DEFAULT NULL COMMENT '谷电累计发电',
  `ordi_gen` decimal(10,2) DEFAULT NULL COMMENT '平电累计发电',
  `peak_gen` decimal(10,2) DEFAULT NULL COMMENT '峰电累计发电',
  `total3` decimal(10,2) DEFAULT NULL COMMENT '光伏累计发电',
  PRIMARY KEY (`did`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ems_data
-- ----------------------------
INSERT INTO `ems_data` VALUES ('1', '402.00', '4.00', '250.00', '87.00', '310.00', '160.00', '51.00', '521.00', '52.00', '102.00', '301.00', '455.00', '0.00', '150.39', '67.00', '217.69');
INSERT INTO `ems_data` VALUES ('2', '321.00', '34.00', '345.00', '78.00', '100.00', '200.00', '50.00', '350.00', '10.00', '20.00', '30.00', '60.00', '0.00', '200.00', '70.00', '270.00');
INSERT INTO `ems_data` VALUES ('3', '500.00', '100.00', '100.00', '200.00', '10.00', '20.10', '32.10', '62.10', '10.00', '20.00', '30.00', '60.00', '0.00', '100.00', '200.00', '300.00');
INSERT INTO `ems_data` VALUES ('4', '100.00', '200.00', '300.00', '50.99', '35.50', '45.50', '50.00', '131.00', '12.00', '13.90', '16.10', '42.00', '0.00', '200.00', '25.90', '225.90');
INSERT INTO `ems_data` VALUES ('5', '12.00', '13.00', '15.00', '30.00', '150.50', '160.50', '100.00', '411.00', '10.00', '20.00', '20.00', '50.00', '0.00', '10.00', '20.00', '30.00');

-- ----------------------------
-- Table structure for equ_log_ac
-- ----------------------------
DROP TABLE IF EXISTS `equ_log_ac`;
CREATE TABLE `equ_log_ac` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `equ_id` varchar(25) NOT NULL COMMENT '设备唯一标识',
  `power` int(11) NOT NULL COMMENT '输出功率',
  `real_time_current` int(11) DEFAULT NULL COMMENT '实时电流，单位：0.01A',
  `real_time_voltage` int(11) DEFAULT NULL COMMENT '实时电压，单位：0.1V',
  `collect_time` datetime NOT NULL COMMENT '采集时间，示例：2019-10-09 13:50:20',
  `collect_timestamp` varchar(15) NOT NULL DEFAULT '0' COMMENT '采集时间戳，示例：1570600220',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_equId_collectTimestamp` (`equ_id`,`collect_timestamp`) COMMENT '同一时刻，采集单设备只会有一条记录'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='设备运行功率日志表(采集服务定时5分钟[可配]采集持久化)';

-- ----------------------------
-- Records of equ_log_ac
-- ----------------------------

-- ----------------------------
-- Table structure for equ_log_citygrid
-- ----------------------------
DROP TABLE IF EXISTS `equ_log_citygrid`;
CREATE TABLE `equ_log_citygrid` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `equ_id` varchar(25) NOT NULL COMMENT '设备唯一标识',
  `power` int(11) NOT NULL COMMENT '输出功率',
  `real_time_current` int(11) DEFAULT NULL COMMENT '实时电流，单位：0.01A',
  `real_time_voltage` int(11) DEFAULT NULL COMMENT '实时电压，单位：0.1V',
  `collect_time` datetime NOT NULL COMMENT '采集时间，示例：2019-10-09 13:50:20',
  `collect_timestamp` varchar(15) NOT NULL COMMENT '采集时间戳，示例：1570600220',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_equId_collectTimestamp` (`equ_id`,`collect_timestamp`) USING BTREE COMMENT '同一时刻，采集单设备只会有一条记录'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='设备运行功率日志表';

-- ----------------------------
-- Records of equ_log_citygrid
-- ----------------------------

-- ----------------------------
-- Table structure for equ_log_dc
-- ----------------------------
DROP TABLE IF EXISTS `equ_log_dc`;
CREATE TABLE `equ_log_dc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `equ_id` varchar(25) NOT NULL COMMENT '设备唯一标识',
  `power` int(11) NOT NULL COMMENT '输出功率',
  `real_time_current` int(11) DEFAULT NULL COMMENT '实时电流，单位：0.01A',
  `real_time_voltage` int(11) DEFAULT NULL COMMENT '实时电压，单位：0.1V',
  `collect_time` datetime NOT NULL COMMENT '采集时间，示例：2019-10-09 13:50:20',
  `collect_timestamp` varchar(15) NOT NULL COMMENT '采集时间戳，示例：1570600220',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_equId_collectTimestamp` (`equ_id`,`collect_timestamp`) USING BTREE COMMENT '同一时刻，采集单设备只会有一条记录'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='设备运行功率日志表';

-- ----------------------------
-- Records of equ_log_dc
-- ----------------------------

-- ----------------------------
-- Table structure for equ_log_highpower
-- ----------------------------
DROP TABLE IF EXISTS `equ_log_highpower`;
CREATE TABLE `equ_log_highpower` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `equ_id` varchar(25) NOT NULL COMMENT '设备唯一标识',
  `power` int(11) NOT NULL COMMENT '输出功率',
  `real_time_current` int(11) DEFAULT NULL COMMENT '实时电流，单位：0.01A',
  `real_time_voltage` int(11) DEFAULT NULL COMMENT '实时电压，单位：0.1V',
  `collect_time` datetime NOT NULL COMMENT '采集时间，示例：2019-10-09 13:50:20',
  `collect_timestamp` varchar(15) NOT NULL COMMENT '采集时间戳，示例：1570600220',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_equId_collectTimestamp` (`equ_id`,`collect_timestamp`) USING BTREE COMMENT '同一时刻，采集单设备只会有一条记录'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='设备运行功率日志表';

-- ----------------------------
-- Records of equ_log_highpower
-- ----------------------------

-- ----------------------------
-- Table structure for equ_log_pv
-- ----------------------------
DROP TABLE IF EXISTS `equ_log_pv`;
CREATE TABLE `equ_log_pv` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `equ_id` varchar(25) NOT NULL COMMENT '设备唯一标识',
  `power` int(11) NOT NULL COMMENT '输出功率',
  `real_time_current` int(11) DEFAULT NULL COMMENT '实时电流，单位：0.01A',
  `real_time_voltage` int(11) DEFAULT NULL COMMENT '实时电压，单位：0.1V',
  `collect_time` datetime NOT NULL COMMENT '采集时间，示例：2019-10-09 13:50:20',
  `collect_timestamp` varchar(15) NOT NULL COMMENT '采集时间戳，示例：1570600220',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_equId_collectTimestamp` (`equ_id`,`collect_timestamp`) USING BTREE COMMENT '同一时刻，采集单设备只会有一条记录'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='设备运行功率日志表';

-- ----------------------------
-- Records of equ_log_pv
-- ----------------------------

-- ----------------------------
-- Table structure for equ_log_storedenergy
-- ----------------------------
DROP TABLE IF EXISTS `equ_log_storedenergy`;
CREATE TABLE `equ_log_storedenergy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `equ_id` varchar(25) NOT NULL COMMENT '设备唯一标识',
  `power` int(11) NOT NULL COMMENT '实时功率（当充电时为输入功率，当放电时为输出功率）',
  `charge_status` tinyint(1) DEFAULT NULL COMMENT '充放电状态，"0":放电，"1":充电（针对储能设备，其他设备返回空字符串）',
  `real_time_current` int(11) DEFAULT NULL COMMENT '实时电流，单位：0.01A',
  `real_time_voltage` int(11) DEFAULT NULL COMMENT '实时电压，单位：0.1V',
  `real_time_soc` int(11) DEFAULT NULL COMMENT '储能-实时SOC，单位：1%',
  `collect_time` datetime NOT NULL COMMENT '采集时间，示例：2019-10-09 13:50:20',
  `collect_timestamp` varchar(15) NOT NULL COMMENT '采集时间戳，示例：1570600220',
  `base_status` varchar(25) DEFAULT NULL COMMENT '基本状态',
  `total_voltage` int(25) DEFAULT NULL COMMENT '总电压',
  `temperature` varchar(25) DEFAULT NULL COMMENT '温度',
  `soc` varchar(25) DEFAULT NULL,
  `cycle_time` int(25) DEFAULT NULL COMMENT '循环次数',
  `max_charge_voltageofpile` int(25) DEFAULT NULL COMMENT '电池组最大充电电压',
  `max_charge_current` int(25) DEFAULT NULL COMMENT '电池组最大充电电流',
  `min_discharge_voltageofpile` int(25) DEFAULT NULL COMMENT '电池组最小放电电压',
  `max_discharge_current` int(25) DEFAULT NULL COMMENT '电池组最大放电电流',
  `soh` varchar(25) DEFAULT NULL,
  `remain_capacity` int(25) DEFAULT NULL COMMENT '剩余容量',
  `charge_capactityofpile` int(25) DEFAULT NULL COMMENT '蓄电池充电电量',
  `discharge_capacityofpile` int(25) DEFAULT NULL COMMENT '蓄电池放电电量',
  `daily_accumulate_charge_capactiy` int(25) DEFAULT NULL COMMENT '当日累积充电量',
  `daily_accumulate_discharge_capactiy` int(25) DEFAULT NULL COMMENT '当日累计放电量',
  `history_accumulate_charge_capacity` int(25) DEFAULT NULL COMMENT '历史累计充电量',
  `history_accumulate_discharge_capacity` int(25) DEFAULT NULL COMMENT '历史累计放电量',
  `force_charge_request_mark` tinyint(5) DEFAULT NULL COMMENT '强充标志位',
  `balance_charge_request_mark` tinyint(5) DEFAULT NULL COMMENT '均充标志位',
  `error_codes1` varchar(25) DEFAULT NULL COMMENT '故障代码1',
  `error_codes2` varchar(25) DEFAULT NULL COMMENT '故障代码2',
  `module_number_in_series` int(11) DEFAULT NULL COMMENT '电池组模块串联数',
  `cell_number_in_series` int(11) DEFAULT NULL COMMENT '电池组单体串联数',
  `charge_forbidden_mark` tinyint(5) DEFAULT NULL COMMENT '禁止充电标志',
  `discharge_forbidden_mark` tinyint(5) DEFAULT NULL COMMENT '禁止放电标志',
  `pile_voltage` varchar(500) DEFAULT NULL COMMENT '电池模块-电压（存储示例:0.01,0.02）',
  `pile_temperature` varchar(500) DEFAULT NULL COMMENT '电池模块温度（存储示例:15,32）',
  `pile_current` varchar(500) DEFAULT NULL COMMENT '电池模块-电流（存储示例:0.01,0.02）',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_equId_collectTimestamp` (`equ_id`,`collect_timestamp`) USING BTREE COMMENT '同一时刻，采集单设备只会有一条记录'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='设备运行功率日志表';

-- ----------------------------
-- Records of equ_log_storedenergy
-- ----------------------------

-- ----------------------------
-- Table structure for job_log
-- ----------------------------
DROP TABLE IF EXISTS `job_log`;
CREATE TABLE `job_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_trace` varchar(25) NOT NULL,
  `exec_time` datetime NOT NULL,
  `rule_req` varchar(255) NOT NULL COMMENT '消峰填谷策略-策略生成基于的数据源（json数据源{源于数据采集服务的数据}）',
  `rule_resp` varchar(255) NOT NULL COMMENT '消峰填谷充放电策略-策略生成结果（储能充放控制,储能供电功率,储能充电功率,光伏输出功率,光伏供电功率{存储示例[json字符串]:{"storedPowerInput":"W10","storedPowerOutput":"W1","storedChargeStatus":0}}）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='消峰填谷充放电策略-执行日志表';

-- ----------------------------
-- Records of job_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_permission`;
CREATE TABLE `sys_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permission_name` varchar(10) NOT NULL COMMENT '权限名称',
  `permission` varchar(10) NOT NULL COMMENT '权限内容（存储数据:btn_userPage_userAddBtn）',
  `create_user` varchar(25) NOT NULL,
  `create_time` datetime NOT NULL,
  `last_update_user` varchar(25) NOT NULL,
  `last_update_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='XX系统权限资源表-后期扩展需要，暂时忽略XX';

-- ----------------------------
-- Records of sys_permission
-- ----------------------------

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(25) NOT NULL COMMENT '角色名称（唯一不可重复）',
  `permissions` varchar(255) NOT NULL COMMENT '角色权限（示例存储数据:btn_userPage_userAddBtn,page_userPage{按钮权限btn开头，页面权限page开头}）',
  `create_user` varchar(25) NOT NULL,
  `create_time` datetime NOT NULL,
  `last_update_user` varchar(25) NOT NULL,
  `last_update_time` datetime NOT NULL,
  PRIMARY KEY (`id`,`role_name`),
  KEY `UK_roleName` (`role_name`) COMMENT '角色唯一'
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='系统角色权限表（记录某个角色的权限资源）';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', '系统管理员', 'page_monitor_dashboard,page_monitor_data,page_runMgr_modeConfig,page_runMgr_dailyMgr,page_userMgr,page_roleMgr', 'sys', '2019-10-19 15:43:01', 'sys', '2019-10-19 15:43:05');
INSERT INTO `sys_role` VALUES ('7', '系统全业务权限', 'Aage_monitor_dashboard,page_monitor_data,page_runMgr_modeConfig,page_runMgr_dailyMgr', 'admin', '2019-10-21 10:58:56', 'admin', '2019-10-21 10:59:15');
INSERT INTO `sys_role` VALUES ('9', 'test01', 'Aage_monitor_dashboard,page_monitor_data,page_runMgr_modeConfig,page_runMgr_dailyMgr', 'admin', '2019-10-22 15:46:31', 'admin', '2019-10-22 15:47:09');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(25) NOT NULL DEFAULT '' COMMENT '用户名',
  `password` varchar(25) NOT NULL DEFAULT '' COMMENT '密码',
  `name` varchar(25) NOT NULL DEFAULT '' COMMENT '姓名',
  `title` varchar(25) DEFAULT '' COMMENT '职位',
  `phone_number` varchar(20) NOT NULL DEFAULT '' COMMENT '手机号',
  `email` varchar(25) DEFAULT '' COMMENT '邮件',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '启用状态，"0":禁用,"1":启用',
  `ref_role_ids` varchar(50) DEFAULT '' COMMENT '用户角色id(用户关联的角色集合,存储示例:3,4,5,7)',
  `create_user` varchar(25) NOT NULL,
  `create_time` datetime NOT NULL,
  `last_update_user` varchar(25) NOT NULL,
  `last_update_time` datetime NOT NULL,
  `is_del` tinyint(1) NOT NULL DEFAULT '0' COMMENT '逻辑删除标识(0:未删除；1:已删除)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_phoneNumber` (`phone_number`) COMMENT '手机号唯一',
  UNIQUE KEY `UK_userName` (`user_name`) COMMENT '用户名唯一'
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8 COMMENT='系统用户表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'admin', '123456', '系统管理员', 'CTO', '18516369668', '979191434@qq.com', '1', '1', 'sys', '2019-10-19 13:45:30', 'sys', '2019-10-22 17:03:12', '0');
INSERT INTO `sys_user` VALUES ('27', 'ack2', '123456Ch23456', '陈大发', 'CTO', '185163696691', 'pcbaby-ch@qq.com', '0', '1', 'admin', '2019-10-20 15:30:55', 'admin', '2019-10-21 18:24:53', '0');
INSERT INTO `sys_user` VALUES ('28', 'john', '123456@123', '陈昊天', 'CTO', '18516369999', 'pcbaby@gg.com', '0', null, 'admin', '2019-10-21 11:43:22', 'admin', '2019-10-21 11:43:22', '0');
INSERT INTO `sys_user` VALUES ('29', 'chent', 'bbfp2306', 'chentao', 'cc', '18021501319', 'pcbaby-ch@qq.com', '1', null, 'admin', '2019-10-22 15:24:11', 'admin', '2019-10-22 16:36:19', '0');
INSERT INTO `sys_user` VALUES ('30', 'chent7', 'bbfp2307', 'chentao07', 'c7777', '18021501318', 'pcbaby-ch@qq.cn', '1', '7', 'admin', '2019-10-22 15:48:45', 'admin', '2019-10-22 16:44:35', '0');
INSERT INTO `sys_user` VALUES ('31', 'chent03', 'bbfp2306', 'chentao', 'cc', '18021501311', 'pcbaby-ch@qq.com', '0', null, 'admin', '2019-10-22 15:52:41', 'admin', '2019-10-22 15:53:58', '1');
INSERT INTO `sys_user` VALUES ('39', 'ack', 'sdf233', '陈钊', 'CTO', '18516369669', 'pcbaby-ch@qq.com', '0', null, 'admin', '2019-10-22 16:04:41', 'admin', '2019-10-22 16:43:17', '0');
