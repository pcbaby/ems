/*
Navicat MySQL Data Transfer

Source Server         : 49.4.10.6_EMS
Source Server Version : 50728
Source Host           : 49.4.10.6:3306
Source Database       : ems

Target Server Type    : MYSQL
Target Server Version : 50728
File Encoding         : 65001

Date: 2019-10-24 23:59:03
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alarm_log
-- ----------------------------
DROP TABLE IF EXISTS `alarm_log`;
CREATE TABLE `alarm_log` (
  `logid` int(11) NOT NULL AUTO_INCREMENT COMMENT '日志表主键',
  `device_type` varchar(255) DEFAULT NULL COMMENT '设备类型',
  `device_num` varchar(255) DEFAULT NULL COMMENT '设备编号',
  `alarm_type` varchar(255) DEFAULT NULL COMMENT '报警类型',
  `alarm_time` date DEFAULT NULL COMMENT '报警时间',
  `device_status` varchar(255) DEFAULT NULL COMMENT '设备状态',
  PRIMARY KEY (`logid`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for config_equ_run
-- ----------------------------
DROP TABLE IF EXISTS `config_equ_run`;
CREATE TABLE `config_equ_run` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) NOT NULL COMMENT '设备类型(cityGrid,storedEnergy,PV,highPower,DC,AC,general)',
  `equ_id` varchar(25) NOT NULL COMMENT '设备唯一标识',
  `switch_status` tinyint(5) NOT NULL COMMENT '开关状态，"0":关，"1":开',
  `max_power` int(11) NOT NULL COMMENT '最大供电功率（额定功率），单位:0.0001kvar',
  `real_time_power` int(11) DEFAULT NULL COMMENT '实时功率，单位:0.0001kvar',
  `real_time_power_useless` int(11) DEFAULT NULL COMMENT '无功功率，单位:0.0001kvar',
  `real_time_power_view` int(11) DEFAULT NULL COMMENT '视在功率，单位:0.0001kvar',
  `real_time_energy` int(11) DEFAULT NULL,
  `real_time_energy_forward` int(11) DEFAULT NULL COMMENT '正向实时电量',
  `real_time_energy_reverse` int(11) DEFAULT NULL COMMENT '反向实时电量',
  `real_time_current0` int(11) DEFAULT NULL COMMENT '光伏-实时发电电流（A相），单位：0.01A',
  `real_time_current1` int(11) DEFAULT NULL COMMENT '光伏-实时发电电流（B相），单位：0.01A',
  `real_time_current2` int(11) DEFAULT NULL COMMENT '光伏-实时发电电流（C相），单位：0.01A',
  `real_time_voltage0` int(11) DEFAULT NULL COMMENT '光伏-实时发电电压（A相）单位：0.1V',
  `real_time_voltage1` int(11) DEFAULT NULL COMMENT '光伏-实时发电电压（B相）单位：0.1V',
  `real_time_voltage2` int(11) DEFAULT NULL COMMENT '光伏-实时发电电压（C相）单位：0.1V',
  `stored_real_time_soc` int(11) DEFAULT NULL COMMENT '储能-实时SOC，单位：1%',
  `stored_real_time_capacity` int(11) DEFAULT NULL,
  `stored_charge_status` tinyint(5) DEFAULT NULL COMMENT '储能-充放电状态(0:充电；1:放电)',
  `stored_max_charge_power` int(11) DEFAULT NULL,
  `stored_max_discharge_power` int(11) DEFAULT NULL,
  `stored_soc` varchar(25) DEFAULT NULL COMMENT '储能-额定SOC，单位：1%',
  `stored_soh` varchar(25) DEFAULT NULL COMMENT '储能-额定SOH，单位：1%',
  `stored_provider` varchar(11) DEFAULT NULL,
  `stored_device_type` varchar(25) DEFAULT NULL,
  `stored_max_engine` int(255) DEFAULT NULL,
  `stored_install_time` varchar(25) DEFAULT NULL,
  `stored_install_position` varchar(50) DEFAULT NULL,
  `stored_grid_voltage` int(11) DEFAULT NULL,
  `stored_grid_current` int(11) DEFAULT NULL,
  `stored_eps_current` int(11) DEFAULT NULL,
  `stored_battery_voltage` int(11) DEFAULT NULL,
  `stored_battery_current` int(11) DEFAULT NULL,
  `stored_daily_charge_power` int(11) DEFAULT NULL,
  `stored_daily_discharge_power` int(11) DEFAULT NULL,
  `stored_total_charge_power` int(11) DEFAULT NULL,
  `stored_total_discharge_power` int(11) DEFAULT NULL,
  `stored_daily_eps_discharge_power` int(11) DEFAULT NULL,
  `stored_total_eps_discharge_power` int(11) DEFAULT NULL,
  `stored_single_device` varchar(500) DEFAULT NULL,
  `pv_capacity` int(11) DEFAULT NULL COMMENT '光伏-装机容量，单位：Wh(暂定)',
  `citygrid_set_power_low` int(11) DEFAULT NULL COMMENT '市网-设定功率（低），单位:0.0001kvar',
  `citygrid_set_power_high` int(11) DEFAULT NULL COMMENT '市网-设定功率（高），单位:0.0001kvar',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_type_equId` (`type`,`equ_id`) USING BTREE COMMENT '每种设备类型下，一台设备只能有一条记录'
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='设备-运行实时数据+配置数据(包含所有设备类型[市电、储能、光伏、DC桩、AC桩、大功率设备、常规负载])';

-- ----------------------------
-- Table structure for config_mode
-- ----------------------------
DROP TABLE IF EXISTS `config_mode`;
CREATE TABLE `config_mode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL COMMENT '模式类型',
  `key` varchar(25) NOT NULL COMMENT '模式-属性key',
  `value` varchar(25) NOT NULL COMMENT '模式-属性值',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_type_key` (`type`,`key`) USING BTREE COMMENT '每种类型下key唯一'
) ENGINE=InnoDB AUTO_INCREMENT=944 DEFAULT CHARSET=utf8 COMMENT='模式配置表';

-- ----------------------------
-- Table structure for ems_data
-- ----------------------------
DROP TABLE IF EXISTS `ems_data`;
CREATE TABLE `ems_data` (
  `did` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `high_power` decimal(10,2) DEFAULT NULL COMMENT '大功率用电',
  `v2l_elec` decimal(10,2) DEFAULT NULL COMMENT 'V2l累计用电',
  `dc_elec` decimal(10,2) DEFAULT NULL COMMENT 'DC桩累计用电',
  `ac_elec` decimal(10,2) DEFAULT NULL COMMENT 'AC桩累计用电',
  `vall_elec` decimal(10,2) DEFAULT NULL COMMENT '谷电累计充电',
  `ordi_tele` decimal(10,2) DEFAULT NULL COMMENT '平电累计充电',
  `peak_elec` decimal(10,2) DEFAULT NULL COMMENT '峰电累计充电',
  `total1` decimal(11,2) DEFAULT NULL COMMENT '储能累计充电',
  `vall_disc` decimal(10,2) DEFAULT NULL COMMENT '谷电累计放电',
  `ordi_disc` decimal(10,2) DEFAULT NULL COMMENT '平电累计放电',
  `peak_disc` decimal(10,2) DEFAULT NULL COMMENT '峰电累计放电',
  `total2` decimal(10,2) DEFAULT NULL COMMENT '储能累计放电',
  `vall_gen` decimal(10,2) DEFAULT NULL COMMENT '谷电累计发电',
  `ordi_gen` decimal(10,2) DEFAULT NULL COMMENT '平电累计发电',
  `peak_gen` decimal(10,2) DEFAULT NULL COMMENT '峰电累计发电',
  `total3` decimal(10,2) DEFAULT NULL COMMENT '光伏累计发电',
  PRIMARY KEY (`did`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for equ_log_ac
-- ----------------------------
DROP TABLE IF EXISTS `equ_log_ac`;
CREATE TABLE `equ_log_ac` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `equ_id` varchar(25) NOT NULL COMMENT '设备唯一标识',
  `power` int(11) NOT NULL COMMENT '输出功率',
  `real_time_current` int(11) DEFAULT NULL COMMENT '实时电流，单位：0.01A',
  `real_time_voltage` int(11) DEFAULT NULL COMMENT '实时电压，单位：0.1V',
  `collect_time` datetime NOT NULL COMMENT '采集时间，示例：2019-10-09 13:50:20',
  `collect_timestamp` varchar(15) NOT NULL DEFAULT '0' COMMENT '采集时间戳，示例：1570600220',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_equId_collectTimestamp` (`equ_id`,`collect_timestamp`) COMMENT '同一时刻，采集单设备只会有一条记录'
) ENGINE=InnoDB AUTO_INCREMENT=215 DEFAULT CHARSET=utf8 COMMENT='设备运行功率日志表(采集服务定时5分钟[可配]采集持久化)';

-- ----------------------------
-- Table structure for equ_log_citygrid
-- ----------------------------
DROP TABLE IF EXISTS `equ_log_citygrid`;
CREATE TABLE `equ_log_citygrid` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `equ_id` varchar(25) NOT NULL COMMENT '设备唯一标识',
  `power` int(11) NOT NULL COMMENT '输出功率',
  `real_time_current` int(11) DEFAULT NULL COMMENT '实时电流，单位：0.01A',
  `real_time_voltage` int(11) DEFAULT NULL COMMENT '实时电压，单位：0.1V',
  `collect_time` datetime NOT NULL COMMENT '采集时间，示例：2019-10-09 13:50:20',
  `collect_timestamp` varchar(15) NOT NULL COMMENT '采集时间戳，示例：1570600220',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_equId_collectTimestamp` (`equ_id`,`collect_timestamp`) USING BTREE COMMENT '同一时刻，采集单设备只会有一条记录'
) ENGINE=InnoDB AUTO_INCREMENT=218 DEFAULT CHARSET=utf8 COMMENT='设备运行功率日志表';

-- ----------------------------
-- Table structure for equ_log_dc
-- ----------------------------
DROP TABLE IF EXISTS `equ_log_dc`;
CREATE TABLE `equ_log_dc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `equ_id` varchar(25) NOT NULL COMMENT '设备唯一标识',
  `power` int(11) NOT NULL COMMENT '输出功率',
  `real_time_current` int(11) DEFAULT NULL COMMENT '实时电流，单位：0.01A',
  `real_time_voltage` int(11) DEFAULT NULL COMMENT '实时电压，单位：0.1V',
  `collect_time` datetime NOT NULL COMMENT '采集时间，示例：2019-10-09 13:50:20',
  `collect_timestamp` varchar(15) NOT NULL COMMENT '采集时间戳，示例：1570600220',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_equId_collectTimestamp` (`equ_id`,`collect_timestamp`) USING BTREE COMMENT '同一时刻，采集单设备只会有一条记录'
) ENGINE=InnoDB AUTO_INCREMENT=216 DEFAULT CHARSET=utf8 COMMENT='设备运行功率日志表';

-- ----------------------------
-- Table structure for equ_log_highpower
-- ----------------------------
DROP TABLE IF EXISTS `equ_log_highpower`;
CREATE TABLE `equ_log_highpower` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `equ_id` varchar(25) NOT NULL COMMENT '设备唯一标识',
  `power` int(11) NOT NULL COMMENT '输出功率',
  `real_time_current` int(11) DEFAULT NULL COMMENT '实时电流，单位：0.01A',
  `real_time_voltage` int(11) DEFAULT NULL COMMENT '实时电压，单位：0.1V',
  `collect_time` datetime NOT NULL COMMENT '采集时间，示例：2019-10-09 13:50:20',
  `collect_timestamp` varchar(15) NOT NULL COMMENT '采集时间戳，示例：1570600220',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_equId_collectTimestamp` (`equ_id`,`collect_timestamp`) USING BTREE COMMENT '同一时刻，采集单设备只会有一条记录'
) ENGINE=InnoDB AUTO_INCREMENT=216 DEFAULT CHARSET=utf8 COMMENT='设备运行功率日志表';

-- ----------------------------
-- Table structure for equ_log_pv
-- ----------------------------
DROP TABLE IF EXISTS `equ_log_pv`;
CREATE TABLE `equ_log_pv` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `equ_id` varchar(25) NOT NULL COMMENT '设备唯一标识',
  `power` int(11) NOT NULL COMMENT '输出功率',
  `real_time_current` int(11) DEFAULT NULL COMMENT '实时电流，单位：0.01A',
  `real_time_voltage` int(11) DEFAULT NULL COMMENT '实时电压，单位：0.1V',
  `collect_time` datetime NOT NULL COMMENT '采集时间，示例：2019-10-09 13:50:20',
  `collect_timestamp` varchar(15) NOT NULL COMMENT '采集时间戳，示例：1570600220',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_equId_collectTimestamp` (`equ_id`,`collect_timestamp`) USING BTREE COMMENT '同一时刻，采集单设备只会有一条记录'
) ENGINE=InnoDB AUTO_INCREMENT=217 DEFAULT CHARSET=utf8 COMMENT='设备运行功率日志表';

-- ----------------------------
-- Table structure for equ_log_storedenergy
-- ----------------------------
DROP TABLE IF EXISTS `equ_log_storedenergy`;
CREATE TABLE `equ_log_storedenergy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `equ_id` varchar(25) NOT NULL COMMENT '设备唯一标识',
  `power` int(11) NOT NULL COMMENT '实时功率（当充电时为输入功率，当放电时为输出功率）',
  `charge_status` tinyint(1) DEFAULT NULL COMMENT '充放电状态，"0":放电，"1":充电（针对储能设备，其他设备返回空字符串）',
  `real_time_current` int(11) DEFAULT NULL COMMENT '实时电流，单位：0.01A',
  `real_time_voltage` int(11) DEFAULT NULL COMMENT '实时电压，单位：0.1V',
  `real_time_soc` int(11) DEFAULT NULL COMMENT '储能-实时SOC，单位：1%',
  `collect_time` datetime NOT NULL COMMENT '采集时间，示例：2019-10-09 13:50:20',
  `collect_timestamp` varchar(15) NOT NULL COMMENT '采集时间戳，示例：1570600220',
  `base_status` varchar(25) DEFAULT NULL COMMENT '基本状态',
  `total_voltage` int(25) DEFAULT NULL COMMENT '总电压',
  `temperature` varchar(25) DEFAULT NULL COMMENT '温度',
  `soc` varchar(25) DEFAULT NULL,
  `cycle_time` int(25) DEFAULT NULL COMMENT '循环次数',
  `max_charge_voltageofpile` int(25) DEFAULT NULL COMMENT '电池组最大充电电压',
  `max_charge_current` int(25) DEFAULT NULL COMMENT '电池组最大充电电流',
  `min_discharge_voltageofpile` int(25) DEFAULT NULL COMMENT '电池组最小放电电压',
  `max_discharge_current` int(25) DEFAULT NULL COMMENT '电池组最大放电电流',
  `soh` varchar(25) DEFAULT NULL,
  `remain_capacity` int(25) DEFAULT NULL COMMENT '剩余容量',
  `charge_capactityofpile` int(25) DEFAULT NULL COMMENT '蓄电池充电电量',
  `discharge_capacityofpile` int(25) DEFAULT NULL COMMENT '蓄电池放电电量',
  `daily_accumulate_charge_capactiy` int(25) DEFAULT NULL COMMENT '当日累积充电量',
  `daily_accumulate_discharge_capactiy` int(25) DEFAULT NULL COMMENT '当日累计放电量',
  `history_accumulate_charge_capacity` int(25) DEFAULT NULL COMMENT '历史累计充电量',
  `history_accumulate_discharge_capacity` int(25) DEFAULT NULL COMMENT '历史累计放电量',
  `force_charge_request_mark` tinyint(5) DEFAULT NULL COMMENT '强充标志位',
  `balance_charge_request_mark` tinyint(5) DEFAULT NULL COMMENT '均充标志位',
  `error_codes1` varchar(25) DEFAULT NULL COMMENT '故障代码1',
  `error_codes2` varchar(25) DEFAULT NULL COMMENT '故障代码2',
  `module_number_in_series` int(11) DEFAULT NULL COMMENT '电池组模块串联数',
  `cell_number_in_series` int(11) DEFAULT NULL COMMENT '电池组单体串联数',
  `charge_forbidden_mark` tinyint(5) DEFAULT NULL COMMENT '禁止充电标志',
  `discharge_forbidden_mark` tinyint(5) DEFAULT NULL COMMENT '禁止放电标志',
  `pile_voltage` varchar(500) DEFAULT NULL COMMENT '电池模块-电压（存储示例:0.01,0.02）',
  `pile_temperature` varchar(500) DEFAULT NULL COMMENT '电池模块温度（存储示例:15,32）',
  `pile_current` varchar(500) DEFAULT NULL COMMENT '电池模块-电流（存储示例:0.01,0.02）',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_equId_collectTimestamp` (`equ_id`,`collect_timestamp`) USING BTREE COMMENT '同一时刻，采集单设备只会有一条记录'
) ENGINE=InnoDB AUTO_INCREMENT=218 DEFAULT CHARSET=utf8 COMMENT='设备运行功率日志表';

-- ----------------------------
-- Table structure for job_log
-- ----------------------------
DROP TABLE IF EXISTS `job_log`;
CREATE TABLE `job_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_trace` varchar(25) NOT NULL,
  `exec_time` datetime NOT NULL,
  `rule_req` varchar(255) NOT NULL COMMENT '消峰填谷策略-策略生成基于的数据源（json数据源{源于数据采集服务的数据}）',
  `rule_resp` varchar(255) NOT NULL COMMENT '消峰填谷充放电策略-策略生成结果（储能充放控制,储能供电功率,储能充电功率,光伏输出功率,光伏供电功率{存储示例[json字符串]:{"storedPowerInput":"W10","storedPowerOutput":"W1","storedChargeStatus":0}}）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='消峰填谷充放电策略-执行日志表';

-- ----------------------------
-- Table structure for sys_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_permission`;
CREATE TABLE `sys_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permission_name` varchar(10) NOT NULL COMMENT '权限名称',
  `permission` varchar(10) NOT NULL COMMENT '权限内容（存储数据:btn_userPage_userAddBtn）',
  `create_user` varchar(25) NOT NULL,
  `create_time` datetime NOT NULL,
  `last_update_user` varchar(25) NOT NULL,
  `last_update_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='XX系统权限资源表-后期扩展需要，暂时忽略XX';

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(25) NOT NULL COMMENT '角色名称（唯一不可重复）',
  `permissions` varchar(255) NOT NULL COMMENT '角色权限（示例存储数据:btn_userPage_userAddBtn,page_userPage{按钮权限btn开头，页面权限page开头}）',
  `create_user` varchar(25) NOT NULL,
  `create_time` datetime NOT NULL,
  `last_update_user` varchar(25) NOT NULL,
  `last_update_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_roleName` (`role_name`) USING BTREE COMMENT '角色唯一'
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='系统角色权限表（记录某个角色的权限资源）';

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(25) NOT NULL DEFAULT '' COMMENT '用户名',
  `password` varchar(25) NOT NULL DEFAULT '' COMMENT '密码',
  `name` varchar(25) NOT NULL DEFAULT '' COMMENT '姓名',
  `title` varchar(25) DEFAULT '' COMMENT '职位',
  `phone_number` varchar(20) NOT NULL DEFAULT '' COMMENT '手机号',
  `email` varchar(25) DEFAULT '' COMMENT '邮件',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '启用状态，"0":禁用,"1":启用',
  `ref_role_ids` varchar(50) DEFAULT '' COMMENT '用户角色id(用户关联的角色集合,存储示例:3,4,5,7)',
  `create_user` varchar(25) NOT NULL,
  `create_time` datetime NOT NULL,
  `last_update_user` varchar(25) NOT NULL,
  `last_update_time` datetime NOT NULL,
  `is_del` tinyint(1) NOT NULL DEFAULT '0' COMMENT '逻辑删除标识(0:未删除；1:已删除)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_phoneNumber` (`phone_number`) COMMENT '手机号唯一',
  UNIQUE KEY `UK_userName` (`user_name`) COMMENT '用户名唯一'
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8 COMMENT='系统用户表';
